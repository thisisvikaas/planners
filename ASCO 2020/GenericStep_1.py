import pandas as pd
import numpy as np

pd.options.mode.chained_assignment = None

# Input Data - Base files
matrix = pd.read_csv('matrix.csv', encoding="latin1", low_memory=False)
kg_data = pd.read_csv('Stable_KG_Mappings.csv', encoding="utf-8", low_memory=False)
trials_db = pd.read_csv('trialdb.csv', encoding="utf-8", low_memory=False)
no_catalyst_matrix = pd.read_csv('nocatalystmatrix.csv', encoding="utf-8", low_memory=False)
sort = pd.read_csv('sort.csv', encoding="latin1", low_memory=False)

# Input Data Manual
planner_raw_data = pd.read_csv('ASCO-raw-data.csv', encoding="utf-8", low_memory=False)
print(planner_raw_data.columns.values)

planner_melt_data = pd.read_csv('planner_melt_data.csv', encoding="latin1", low_memory=False)
planner_annotation_data = pd.read_csv('planner_annotation_data.csv', encoding="latin1", low_memory=False)
filter = pd.read_csv('filter.csv', encoding="latin1", low_memory=False)

#All diseases in annotation
disease_input_id = ("DIS000062","DIS001286","DIS000039","DIS000028","DIS000331","DIS000766","DIS000856","DIS001168","DIS000033","DIS000579","DIS000002","DIS004355","DIS000083","DIS000407","DIS000030","DIS004967","DIS001980","DIS000032","DIS000052","DIS000374","DIS000649","DIS000386","DIS000259","DIS000034","DIS000264","DIS001769","DIS000043","DIS000071","DIS000045","DIS002199","DIS001606","DIS000165","DIS000101","DIS000069","DIS000067","DIS000082","DIS000831","DIS001050","DIS001005","DIS000232","DIS000278","DIS000394","DIS000123","DIS000485","DIS000031","DIS000048","DIS000099","DIS000137","DIS000213","DIS000007","DIS001322","DIS000093","DIS000081","DIS000042","DIS000070","DIS000222","DIS000138","DIS005002","DIS000596","DIS001390","DIS000594","DIS003000","DIS000152","DIS000094","DIS000125","DIS000459","DIS001768","DIS000203","DIS000063","DIS000934","DIS000369","DIS001120","DIS001495","DIS000108","DIS000225","DIS005010","DIS001308","DIS000112","DIS001923","DIS000056","DIS000174","DIS000100","DIS000049","DIS000661","DIS000074","DIS000724","DIS000326","DIS000630","DIS000739","DIS000086","DIS000087","DIS000350","DIS000107","DIS001595","DIS000862","DIS001409","DIS000475","DIS000377","DIS000743","DIS000329","DIS001161","DIS000164","DIS000171","DIS000320","DIS001175","DIS000313","DIS000020","DIS000598","DIS000684","DIS000058","DIS000085","DIS000808","DIS001224","DIS000853","DIS002300","DIS000131","DIS000412","DIS000997","DIS001385","DIS001704","DIS001568","DIS000111","DIS000089","DIS002289","DIS000588","DIS000747","DIS000443","DIS001794","DIS001425","DIS000532","DIS000156","DIS000563","DIS000748","DIS000408","DIS001051","DIS000498","DIS000792","DIS000584","DIS000116","DIS001959","DIS000676","DIS000121","DIS001047","DIS004809","DIS000295","DIS000510","DIS000398","DIS000105","DIS001437","DIS001297","DIS000623","DIS000140","DIS000420","DIS000795","DIS000268","DIS000279","DIS000035","DIS000378","DIS002169","DIS000029","DIS004909","DIS002007","DIS004974","DIS000527","DIS002304","DIS000603","DIS000886","DIS000414","DIS000492","DIS001144","DIS000399","DIS000293","DIS000055","DIS004895","DIS000242","DIS000520","DIS000575","DIS000730","DIS002340","DIS001134","DIS001056","DIS000077","DIS000501","DIS000774","DIS000258","DIS000257","DIS000289","DIS001125","DIS000818","DIS000372","DIS000798","DIS002797","DIS000616","DIS000090","DIS000172","DIS000480","DIS000828","DIS000860","DIS002814","DIS000298","DIS002839","DIS002012","DIS000837","DIS000075","DIS000585","DIS000261","DIS000582","DIS000041","DIS000656","DIS000219","DIS000161","DIS000054","DIS000327","DIS000155","DIS000415","DIS000160","DIS000633","DIS000651","DIS000416","DIS000117","DIS001935","DIS004952","DIS000209","DIS000147","DIS000733","DIS000441","DIS001417","DIS000379","DIS001635","DIS000286","DIS000357","DIS000424","DIS000285","DIS001036","DIS000246","DIS000430","DIS001742","DIS004902","DIS000271","DIS000178","DIS000068","DIS002505","DIS001775","DIS002358","DIS000312","DIS000104","DIS004371","DIS000982","DIS000012","DIS000710","DIS000356","DIS002147","DIS001209","DIS002054","DIS002977","DIS001242","DIS003240","DIS003243","DIS001820","DIS000221","DIS001337","DIS000643","DIS000375","DIS002004","DIS000269","DIS000620","DIS000341","DIS000512","DIS000567","DIS000231","DIS000422","DIS000840","DIS000401","DIS001928","DIS001570","DIS000019","DIS000376","DIS000244","DIS001777","DIS001453","DIS000690","DIS000706","DIS001758","DIS001609","DIS002220","DIS002365","DIS000953","DIS001132","DIS001921","DIS000366","DIS000352","DIS000969","DIS001350","DIS001556","DIS005011","DIS000273","DIS000097","DIS000206","DIS000784","DIS000079","DIS000205","DIS004969","DIS000346","DIS000229","DIS005000","DIS001069","DIS001011","DIS000755","DIS001706","DIS001317","DIS000228","DIS000328","DIS001261","DIS003253","DIS001502","DIS001160","DIS000200")
print(disease_input_id)

TA_input = 'Oncology'
TA_input_id = 'DIS000002'
print(TA_input_id)

# Filter KG and Trial data based on Disease Input
def get_filtered_df(source, input, field):
    source_data_filtered = pd.DataFrame()
    for x in input:
        temp_source_data = source[source[field] == x]
        source_data_filtered = source_data_filtered.append(temp_source_data)
    return source_data_filtered


kg_data = get_filtered_df(kg_data, disease_input_id, "k_disease_id")
kg_data.to_csv(r'filteredKG.csv', index = False)

trials_db = get_filtered_df(trials_db, disease_input_id,"disease_id")
trials_db.to_csv(r'filteredTRIAL.csv', index = False)


# Create list of Source IDs and TAs
def get_TA():
    planner_TA = planner_annotation_data[planner_annotation_data['source_id'] == TA_input_id][["Internal ID", "name"]]
    planner_TA.rename(columns={'name': 'TA'}, inplace=True)
    return planner_TA


# Create a list of Source IDs and Diseases
def get_all_indications():
    planner_indications = planner_annotation_data[planner_annotation_data['label'] == 'Disease'][
        ["Internal ID", "source_id"]]
    planner_indications.rename(columns={'source_id': 'indication_id'}, inplace=True)
    return planner_indications


# Create a matrix based on values in a row
def generateMatrix(interimCol, finalCol):
    planner[interimCol] = planner['label'].apply(lambda x: 1 if x == finalCol else 0)
    planner[finalCol] = planner.groupby('Internal ID')[interimCol].transform(sum)
    planner[finalCol] = planner[finalCol].apply(lambda x: 1 if x > 0 else 0)
    return planner[finalCol]


# Columns into comma seperated values
def get_kg_entities():
    disease = planner_drug_disease[planner_drug_disease['label'] == 'Disease'][['Internal ID', 'name']]
    disease['Disease'] = disease.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    disease = disease[['Internal ID', 'Disease']].drop_duplicates()

    drug = planner_drug_disease[planner_drug_disease['label'] == 'Drug'][['Internal ID', 'name']]
    drug['Drug'] = drug.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    drug = drug[['Internal ID', 'Drug']].drop_duplicates()

    agency = planner_drug_disease[planner_drug_disease['label'] == 'Agency'][['Internal ID', 'name']]
    agency['Agency'] = agency.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    agency = agency[['Internal ID', 'Agency']].drop_duplicates()

    drug_classes = planner_drug_disease[planner_drug_disease['label'] == 'MOA'][['Internal ID', 'name']]
    drug_classes['Drug Class'] = drug_classes.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    drug_classes = drug_classes[['Internal ID', 'Drug Class']].drop_duplicates()

    patient_subgroups = planner_drug_disease[planner_drug_disease['label'] == 'PSUB'][['Internal ID', 'name']]
    patient_subgroups['Patient Subgroup'] = patient_subgroups.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    patient_subgroups = patient_subgroups[['Internal ID', 'Patient Subgroup']].drop_duplicates()


    return disease, drug, agency, drug_classes,patient_subgroups


# Show from where annotation Picked up the disease
def getDiseaseSource():
    diseasesource = pd.DataFrame()
    for x in disease_input_id:
        temp_df = planner_annotation_data[planner_annotation_data['source_id'] == x]
        diseasesource = diseasesource.append(temp_df)

    diseasesource = diseasesource[['Internal ID', 'field']]
    diseasesource['source'] = diseasesource.groupby(['Internal ID'])['field'].transform(lambda x: ', '.join(x))
    diseasesource = diseasesource[['Internal ID', 'source']].drop_duplicates()

    return diseasesource


# Generate single row matrix from annotation
def generateMatrixANNO(interimCol, finalCol):
    planner_raw_data_annotation[interimCol] = planner_raw_data_annotation['label'].apply(
        lambda x: 1 if x == finalCol else 0)
    planner_raw_data_annotation[finalCol] = planner_raw_data_annotation.groupby('Internal ID')[interimCol].transform(
        sum)
    planner_raw_data_annotation[finalCol] = planner_raw_data_annotation[finalCol].apply(lambda x: 1 if x > 0 else 0)

    return planner_raw_data_annotation[finalCol]


# Generate single row catalyst matrix
def generate_catalyst_matrix(interimCol, finalCol):
    planner_melt_data[interimCol] = planner_melt_data['Catalyst Type/Topics'].apply(lambda x: 1 if x == finalCol else 0)
    planner_melt_data[finalCol] = planner_melt_data.groupby('Internal ID')[interimCol].transform(sum)
    planner_melt_data[finalCol] = planner_melt_data[finalCol].apply(lambda x: 1 if x > 0 else 0)
    return planner_melt_data[finalCol]


# Generate catalyst priority matrix
def generate_catalyst_priority():
    planner_melt_data['Trial Readouts'] = generate_catalyst_matrix('I_Trial Readouts', 'Trial Readouts')
    planner_melt_data['Clinical Trial Details'] = generate_catalyst_matrix('I_Clinical Trial Details',
                                                                           'Clinical Trial Details')
    planner_melt_data['FDA Review Outcomes'] = generate_catalyst_matrix('I_FDA Review Outcomes',
                                                                        'FDA Review Outcomes')
    planner_melt_data['Endpoints'] = generate_catalyst_matrix('I_Endpoints', 'Endpoints')
    planner_melt_data['KOL'] = generate_catalyst_matrix('I_KOL', 'KOL')
    planner_melt_data['New Entries'] = generate_catalyst_matrix('I_New Entries', 'New Entries')
    planner_melt_data['Recruitment status'] = generate_catalyst_matrix('I_Recruitment status',
                                                                       'Recruitment status')
    planner_melt_data['Educational and Research'] = generate_catalyst_matrix(
        'I_Educational and Research', 'Educational and Research')
    planner_melt_data['Disease Statistic'] = generate_catalyst_matrix('I_Disease Statistic',
                                                                      'Disease Statistic')
    planner_melt_data['Treatment Details'] = generate_catalyst_matrix('I_Treatment Details',
                                                                      'Treatment Details')
    planner_melt_data['Drug Application'] = generate_catalyst_matrix('I_Drug Application', 'Drug Application')
    planner_melt_data['Preclinical'] = generate_catalyst_matrix('I_Preclinical', 'Preclinical')
    planner_melt_data['Late-Breaking Abstracts'] = generate_catalyst_matrix('I_Late-Breaking Abstracts',
                                                                            'Late-Breaking Abstracts')
    return planner_melt_data


# v2 Priortization Logic
def finalPriority(planner_raw_data):
    if planner_raw_data['Updated_Relevance'] == 1:
        if planner_raw_data['v1Priority'] != 'Ignore' and planner_raw_data['v1Priority'] != 'Not Relevant':
            return planner_raw_data['v1Priority']
        else:
            return planner_raw_data['NoCatalystPriority']
    else:
        # Add TA in the casing present in TA mapping file
        if planner_raw_data['TA'] == TA_input:
            if planner_raw_data['NoCatalystPriority'] == "Not Relevant":
                return "Ignore"
            elif planner_raw_data['NoCatalystPriority'] is not np.nan:
                return planner_raw_data['NoCatalystPriority']
            else:
                return "Ignore"
        else:
            return "Not Relevant"


# Convert single row
def singleRow(col):
    planner_raw_data[col] = planner_raw_data.groupby('Internal ID')[col].transform(sum)
    planner_raw_data[col] = planner_raw_data[col].apply(lambda x: 1 if x > 0 else 0)
    return planner_raw_data[col]


# Perform Generic OR Brand
def f(planner_raw_data):
    return planner_raw_data['CheckedGeneric'] or planner_raw_data['CheckedBrand']


# Filter required columns
planner = planner_raw_data[['Internal ID','Abstract ID','Session Type', 'Session Name', 'Abstract Title', 'Session Start Date','Author','Institution']]

# Add Relevance Based on Input Disease Area
planner_indications = get_all_indications()
planner_raw_data = planner_raw_data.merge(planner_indications, on='Internal ID', how='left')
planner_raw_data['Relevance'] = planner_raw_data['indication_id'].apply(lambda x: 1 if x in disease_input_id else 0)
planner_raw_data.sort_values(by='Relevance', ascending=False, inplace=True)
planner_raw_data.drop_duplicates(subset='Internal ID', keep='first', inplace=True)

# Generate Catalyst Priority
planner_melt_data = generate_catalyst_priority()
planner_melt_data = planner_melt_data[
    ['Internal ID', 'Catalyst Type/Topics', 'Trial Readouts', 'Clinical Trial Details', 'FDA Review Outcomes',
     'Endpoints', 'KOL', 'New Entries', 'Recruitment status', 'Educational and Research',
     'Disease Statistic', 'Treatment Details', 'Drug Application', 'Preclinical', 'Late-Breaking Abstracts']]
planner_melt_data.drop_duplicates(
    subset={'Internal ID', 'Trial Readouts', 'Clinical Trial Details', 'FDA Review Outcomes', 'Endpoints', 'KOL',
            'New Entries', 'Recruitment status', 'Educational and Research',
            'Disease Statistic', 'Treatment Details', 'Drug Application', 'Preclinical', 'Late-Breaking Abstracts'},
    keep='first', inplace=True)
planner_melt_data = planner_melt_data.merge(matrix,
                                            on=['Trial Readouts', 'Clinical Trial Details', 'FDA Review Outcomes',
                                                'Endpoints', 'KOL',
                                                'New Entries', 'Recruitment status', 'Educational and Research',
                                                'Disease Statistic', 'Treatment Details', 'Drug Application',
                                                'Preclinical',
                                                'Late-Breaking Abstracts'], how='left')

# Add Catalyst Priority & v1Priority
planner_raw_data = planner_raw_data.merge(planner_melt_data, on='Internal ID', how='left')
planner_raw_data.rename(columns={'Priority': 'Catalyst_Priority'}, inplace=True)
planner_raw_data.to_csv(r'checkCatalystPriority.csv', index = False)

planner_raw_data['v1Priority'] = np.where(planner_raw_data.Relevance == 1, planner_raw_data.Catalyst_Priority,
                                          "Not Relevant")
planner_raw_data['v1Priority'] = np.where(planner_raw_data.v1Priority != " ", planner_raw_data.v1Priority, "Ignore")
planner_raw_data['v1Priority'].replace(np.nan, "Ignore", inplace=True)
planner_raw_data = planner_raw_data.loc[:,
                   ['Internal ID','Abstract ID','Session Type', 'Session Name', 'Abstract Title', 'Session Start Date','Author','Institution', 'Relevance',
                    'Catalyst_Priority', 'v1Priority']]

# Attach TA
planner_TA = get_TA()
planner_raw_data = planner_raw_data.merge(planner_TA, on='Internal ID', how='left')

# Add Annotation Results
planner = planner.merge(planner_annotation_data, on='Internal ID', how='left')
planner['Disease'] = generateMatrix('S_Diseases', 'Disease')
planner['Drug'] = generateMatrix('S_Drugs', 'Drug')
planner['Firm'] = generateMatrix('S_Firms', 'Firms')
planner['Trial'] = generateMatrix('S_Trials', 'Trial')
planner['DrugClass'] = generateMatrix('S_DrugClass', 'MOA')

# Get All KG entities
planner_drug_disease = planner[['Internal ID', 'name', 'label']]
A_Diseases, A_Drugs, A_Agency, A_DrugClasses,A_PatientSubgroups = get_kg_entities()

# Attach Trial Data
planner_annotation_data_trials = planner_annotation_data[planner_annotation_data['label'] == 'Trial']
planner_raw_data = planner_raw_data.merge(planner_annotation_data_trials, on='Internal ID', how='left')
planner_raw_data = planner_raw_data.merge(trials_db, left_on='source_id', right_on='nct_id', how='left')
planner_raw_data['drug_id'].fillna(0, inplace=True)
planner_raw_data['drugclass_id'].fillna(0, inplace=True)
planner_raw_data['firm_id'].fillna(0, inplace=True)

# Check if attached trial data is the one present in KG
planner_raw_data = planner_raw_data.assign(CheckedDisease=planner_raw_data.disease_id.isin(kg_data.k_disease_id))
planner_raw_data = planner_raw_data.assign(CheckedGeneric=planner_raw_data.drug_id.isin(kg_data.k_generic_id))
planner_raw_data = planner_raw_data.assign(CheckedBrand=planner_raw_data.drug_id.isin(kg_data.k_brand_id))
planner_raw_data = planner_raw_data.assign(CheckedFirm=planner_raw_data.firm_id.isin(kg_data.k_firm_id))
planner_raw_data = planner_raw_data.assign(CheckedDrugclass=planner_raw_data.drugclass_id.isin(kg_data.k_drug_class_id))

# Keep the field as it is if disease of interest else 0
planner_raw_data['CheckedGeneric'] = np.where(planner_raw_data["CheckedDisease"] == True,
                                              planner_raw_data["CheckedGeneric"], 0)
planner_raw_data['CheckedBrand'] = np.where(planner_raw_data["CheckedDisease"] == True,
                                            planner_raw_data["CheckedBrand"], 0)
planner_raw_data['CheckedFirm'] = np.where(planner_raw_data["CheckedDisease"] == True, planner_raw_data["CheckedFirm"],
                                           0)
planner_raw_data['CheckedDrugclass'] = np.where(planner_raw_data["CheckedDisease"] == True,
                                                planner_raw_data["CheckedDrugclass"], 0)

# Change True to 1 and False to 0
planner_raw_data['CheckedDisease'] = planner_raw_data['CheckedDisease'].apply(lambda x: 1 if x == True else 0)
planner_raw_data['CheckedFirm'] = planner_raw_data['CheckedFirm'].apply(lambda x: 1 if x == True else 0)
planner_raw_data['CheckedGeneric'] = planner_raw_data['CheckedGeneric'].apply(lambda x: 1 if x == True else 0)
planner_raw_data['CheckedBrand'] = planner_raw_data['CheckedBrand'].apply(lambda x: 1 if x == True else 0)
planner_raw_data['CheckedDrugclass'] = planner_raw_data['CheckedDrugclass'].apply(lambda x: 1 if x == True else 0)

# Single out the row and sort then Keep the field as it is if disease of interest else 0
planner_raw_data['CheckedDisease'] = singleRow('CheckedDisease')
planner_raw_data['CheckedFirm'] = singleRow('CheckedFirm')
planner_raw_data['CheckedFirm'] = np.where(planner_raw_data["CheckedDisease"] == 1, singleRow('CheckedFirm'), 0)
planner_raw_data['CheckedGeneric'] = singleRow('CheckedGeneric')
planner_raw_data['CheckedGeneric'] = np.where(planner_raw_data["CheckedDisease"] == 1, singleRow('CheckedGeneric'), 0)
planner_raw_data['CheckedBrand'] = singleRow('CheckedBrand')
planner_raw_data['CheckedBrand'] = np.where(planner_raw_data["CheckedDisease"] == 1, singleRow('CheckedBrand'), 0)
planner_raw_data['CheckedDrugclass'] = singleRow('CheckedDrugclass')
planner_raw_data['CheckedDrugclass'] = np.where(planner_raw_data["CheckedDisease"] == 1, singleRow('CheckedDrugclass'),
                                                0)

planner_raw_data['CheckedDrug'] = planner_raw_data.apply(f, axis=1)
planner_raw_data['CheckedDrug'] = planner_raw_data['CheckedDrug'].apply(lambda x: 1 if x == True else 0)
planner_raw_data['CheckedDrug'] = singleRow('CheckedDrug')

planner_raw_data['CheckedTrial'] = planner_raw_data['disease_id'].apply(lambda x: 0 if x not in disease_input_id else 1)
planner_raw_data['DiseaseFromTrialCheck'] = planner_raw_data['disease_id'].apply(
    lambda x: 1 if x in disease_input_id else 0)
planner_raw_data.sort_values(['DiseaseFromTrialCheck', 'CheckedTrial', 'firm_id', 'drug_id', 'drugclass_id'],
                             ascending=[False, False, False, False, False], inplace=True)
planner_raw_data.drop_duplicates(subset={'Internal ID'}, inplace=True, keep='first')

# If disease of interest keep else np.nan
planner_raw_data['Disease'] = np.where(planner_raw_data.DiseaseFromTrialCheck == 1, planner_raw_data.disease, np.nan)
planner_raw_data['Agency'] = np.where(planner_raw_data.DiseaseFromTrialCheck == 1, planner_raw_data.firm, np.nan)
planner_raw_data['Drug'] = np.where(planner_raw_data.DiseaseFromTrialCheck == 1, planner_raw_data.drug, np.nan)
planner_raw_data['acronym'] = np.where(planner_raw_data.DiseaseFromTrialCheck == 1, planner_raw_data.acronym, np.nan)
planner_raw_data['DrugClass'] = np.where(planner_raw_data.DiseaseFromTrialCheck == 1, planner_raw_data.drugclass,
                                         np.nan)

print(planner_raw_data.columns.values)
planner_raw_data.to_csv(r'checkAbstractIDissue.csv', index = False)
planner_raw_data = planner_raw_data[['Internal ID','Abstract ID_x','Session Type', 'Session Name', 'Abstract Title', 'Session Start Date','Author','Institution', 'Relevance',
     'Catalyst_Priority', 'v1Priority', 'TA', 'Disease', 'Agency', 'Drug', 'DrugClass', 'acronym', 'CheckedDisease',
     'CheckedFirm', 'CheckedDrug', 'CheckedDrugclass', 'CheckedTrial', 'DiseaseFromTrialCheck']]
planner_raw_data.rename(columns={'Disease': 'DiseaseFromTrials', 'Agency': 'FirmFromTrials', 'Drug': 'DrugFromTrials',
                                 'acronym': 'Trial_Acronym', 'DrugClass': 'DrugClassFromTrials'}, inplace=True)

# Append & Filter Annotation Results
planner_raw_data = planner_raw_data.merge(planner_annotation_data, on='Internal ID', how='left')
planner_raw_data_annotation = planner_raw_data
planner_raw_data_annotation = planner_raw_data_annotation[
    (planner_raw_data_annotation.source_id.isin(filter.source_id)) & (
        planner_raw_data_annotation.label.isin(filter.label))]

planner_raw_data_annotation['A_Disease'] = generateMatrixANNO('S_Diseases', 'Disease')
planner_raw_data_annotation['A_Drug'] = generateMatrixANNO('S_Drugs', 'Drug')
planner_raw_data_annotation['A_Firm'] = generateMatrixANNO('S_Firms', 'Agency')
planner_raw_data_annotation['A_Trial'] = generateMatrixANNO('S_Trials', 'Trial')
planner_raw_data_annotation['A_DrugClass'] = generateMatrixANNO('A_DrugClass', 'MOA')
planner_raw_data_annotation.sort_values(by='Trial', ascending=False, inplace=True)
planner_raw_data_annotation.drop_duplicates(subset={'Internal ID'}, inplace=True)

# Perform Annotation OR Trial Data
planner_raw_data_annotation[
    'Final_Disease'] = planner_raw_data_annotation.A_Disease | planner_raw_data_annotation.CheckedDisease
planner_raw_data_annotation['Final_Drug'] = planner_raw_data_annotation.A_Drug | planner_raw_data_annotation.CheckedDrug
planner_raw_data_annotation['Final_Firm'] = planner_raw_data_annotation.A_Firm | planner_raw_data_annotation.CheckedFirm
planner_raw_data_annotation[
    'Final_Trial'] = planner_raw_data_annotation.A_Trial | planner_raw_data_annotation.CheckedTrial
planner_raw_data_annotation[
    'Final_DrugClass'] = planner_raw_data_annotation.A_DrugClass | planner_raw_data_annotation.CheckedDrugclass

planner_raw_data_annotation.drop_duplicates(subset={'Internal ID'}, inplace=True)
planner_raw_data_annotation.rename(
    columns={'Final_Disease': 'NC_Disease', 'Final_Drug': 'NC_Drug', 'Final_Firm': 'NC_Firm', 'Final_Trial': 'NC_Trial',
             'Final_DrugClass': 'NC_DrugClass'}, inplace=True)

# Generate Priority
planner_raw_data_annotation = planner_raw_data_annotation.merge(no_catalyst_matrix,
                                                                on=['NC_Disease', 'NC_Drug', 'NC_Firm', 'NC_Trial',
                                                                    'NC_DrugClass'], how='left')
planner_raw_data_annotation.drop_duplicates(subset={'Internal ID'}, inplace=True)
planner_raw_data_annotation = planner_raw_data_annotation.loc[:,
                              ['Internal ID', 'Priority', 'NC_Disease', 'NC_Drug', 'NC_Firm', 'NC_Trial',
                               'NC_DrugClass']]

# Merge planner_raw_data_annotation with planner_raw_data
planner_raw_data = planner_raw_data.loc[:,
                   ['Internal ID','Abstract ID_x','Session Type', 'Session Name', 'Abstract Title', 'Session Start Date','Author','Institution', 'Relevance',
                    'Catalyst_Priority', 'v1Priority', 'TA', 'DiseaseFromTrials', 'FirmFromTrials', 'DrugFromTrials',
                    'DrugClassFromTrials', 'Trial_Acronym']]
planner_raw_data = planner_raw_data.merge(planner_raw_data_annotation, on='Internal ID', how='left')
planner_raw_data.drop_duplicates(subset={'Internal ID'}, inplace=True)
planner_raw_data.rename(columns={'Priority': 'NoCatalystPriority'}, inplace=True)

# Update Disease Relevance Post Implicit Trial Connections
planner_raw_data['CheckedDiseaseFromTrial'] = planner_raw_data['DiseaseFromTrials'].apply(
    lambda x: 1 if x in disease_input_id else 0)
planner_raw_data['Updated_Relevance'] = planner_raw_data['Relevance'] | planner_raw_data['CheckedDiseaseFromTrial']

# Apply v2 Priortization logic
planner_raw_data['v2Priority'] = planner_raw_data.apply(finalPriority, axis=1)

# Append Annotation Outputs - A_Diseases, A_Drugs, A_Agency & A_DrugClasses
planner_raw_data = pd.merge(planner_raw_data, A_Diseases, 'left', 'Internal ID')
planner_raw_data = pd.merge(planner_raw_data, A_Drugs, 'left', 'Internal ID')
planner_raw_data = pd.merge(planner_raw_data, A_Agency, 'left', 'Internal ID')
planner_raw_data = pd.merge(planner_raw_data, A_DrugClasses, 'left', 'Internal ID')
planner_raw_data = pd.merge(planner_raw_data, A_PatientSubgroups, 'left', 'Internal ID')

# Append disease source from annotation
disease_source = getDiseaseSource()
planner_raw_data = planner_raw_data.merge(disease_source, on='Internal ID', how='left')

# Sort & Export Planner
planner_raw_data = planner_raw_data.merge(sort, on='v2Priority', how='left')
planner_raw_data.sort_values(by='Sort', ascending=True, inplace=True)
planner_raw_data.to_csv(r'step1_planner.csv', index=False)
print(planner_raw_data['Internal ID'].count())
print(planner_raw_data['v2Priority'].count())


