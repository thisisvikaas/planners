import pandas as pd
import re


df = pd.read_csv('planner_raw_data_TB.csv', encoding="utf-8",low_memory=False)
print(df.columns.values)


def strip_character(dataCol):
    r = re.compile(r'[^a-zA-Z0-9 !@#$%&;*_+-=|\:";<>,./()[\]{}\']')
    return r.sub(' ', dataCol)

def strip_col(datacol):
    return datacol.strip()

my_columns = ["Title","Authors","Research Funding","Background","Methods","Results","Conclusions","First Author","Meeting","Session Type","Session Title","Track","Subtrack","Clinical Trial Registry Number","Conclusion_E"]

df.fillna("empty123",inplace = True)

for col in my_columns:
    df[col] = df[col].apply(strip_character)
    df[col] = df[col].apply(strip_col)


df.replace("empty123","",inplace=True)

df.to_csv(r'ASCO-cleaned-v2.csv', index = False)
