import pandas as pd
import re


df = pd.read_csv('asco_chats.csv', encoding="utf-8",low_memory=False)
print(df.columns.values)

df.replace("‚",";",inplace=True)

def strip_character(dataCol):
    r = re.compile(r'[^a-zA-Z0-9 !@#$%&;*_+-=|\:";<>,./()[\]{}\']')
    return r.sub('', dataCol)


my_columns = ["Name ","Question","name_y","comment"]

df.fillna("empty123",inplace = True)

for col in my_columns:
    df[col] = df[col].apply(strip_character)

# df['C_Title'] = df['Title'].apply(strip_character)
# df['C_Authors'] = df['Authors'].apply(strip_character)
# df['C_Institutions'] = df['Institutions'].apply(strip_character)
# df['C_Abstract'] = df['Abstract'].apply(strip_character)
# df['C_Category'] = df['Category'].apply(strip_character)
# #

df.replace("empty123","",inplace=True)

df.to_csv(r'ASCO-cleaned-v2.csv', index = False)
