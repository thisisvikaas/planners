import pandas as pd
import numpy as np
import Planner_Step_1

# Input
final_planner = pd.read_csv(r'step1_planner.csv', encoding="utf-8")
#filter = pd.read_csv('filter.csv', encoding="latin1",low_memory=False)
kg_data = Planner_Step_1.kg_data

final_planner.fillna("nan",inplace = True)

# Append generic and brand drug ids
kg_generic = kg_data['k_generic']
kg_brand = kg_data['k_brand']
k_drug = kg_brand.append(kg_generic)
#filter['label'].replace(['Brand', 'Generic'], 'Drug', inplace=True)


# Combine Annotation + Trial Data
def combine_annotation_trials(df,afield,tfield):
    #df[afield] = df[afield].str.replace(", ","$$")
    return np.where(df[afield]=="nan",df[tfield],
                                            np.where(df[tfield]=="nan",df[afield],
                                                     np.where(df[afield]==df[tfield],df[afield],
                                                              df[[afield, tfield]].apply(lambda x: ', '.join(x), axis=1))))

def filter_by_kg(df,field,kg_field):
    matched_field = df[field].str.split(', ', expand=True)
    matched_field['Internal ID'] = df['Internal ID']
    matched_field = pd.melt(matched_field, id_vars='Internal ID')
    matched_field = matched_field[~matched_field['value'].isnull()]
    matched_field = pd.merge(df, matched_field, 'left', 'Internal ID')

    matched_field['value'] = matched_field['value'].str.strip()
    matched_field.drop_duplicates(subset={"Internal ID", "value"}, inplace=True)
    matched_field = matched_field.dropna()

    filtered_field = matched_field
    #filtered_field = matched_field[(matched_field.value.str.lower().isin(filter.name.str.lower()))]
    filtered_field = filtered_field[['Internal ID', 'value']]

    field_name = "Filtered" + field
    print(field_name)
    filtered_field = filtered_field.rename(columns={"value": field_name})
    filtered_field = filtered_field.groupby(["Internal ID"])[field_name].agg([(field_name, ', '.join)]).reset_index()
    filtered_field.to_csv(f"fiter-{field_name}.csv", index=False)

    return filtered_field

# Filter the Appended KG entities
def filter_all_fields():
    FilteredDisease = filter_by_kg(final_planner, 'AppendedDisease', 'k_disease')
    FilteredDrug = filter_by_kg(final_planner, 'AppendedDrug', 'k_drug')
    FilteredFirm = filter_by_kg(final_planner, 'AppendedFirm', 'k_firm')
    FilteredDrugClass = filter_by_kg(final_planner, 'AppendedDrugClass', 'k_drug_class')
    FilterAgency = filter_by_kg(final_planner,"Agency","k_firm")
    FilterPatientSubgroup = filter_by_kg(final_planner,"Patient Subgroup","k_firm")

    return FilteredDisease, FilteredDrug, FilteredFirm, FilteredDrugClass,FilterAgency,FilterPatientSubgroup


def get_field_weights(source, final_planner, field,type):
    value_source = source[source['label'] == type].filter(["name", "weight"]).drop_duplicates().sort_values(
        by="name")

    field_planner_data = final_planner.filter(["Internal ID", field])
    field_planner_data[field] = field_planner_data[field].fillna("")
    field_planner_data[field] = field_planner_data[field].apply(lambda x: x.split("$$"))
    field_planner_data = field_planner_data.explode(field)
    field_planner_data[field] = field_planner_data[field].apply(lambda x: x.strip(" "))
    field_planner_data = pd.merge(field_planner_data, value_source, how="left", left_on=field, right_on="name")
    field_planner_data["weight"] = field_planner_data["weight"].fillna(0)
    field_planner_data = field_planner_data.groupby("Internal ID")[["Internal ID", "weight"]].sum()
    field_planner_data["Internal ID"] = field_planner_data.index
    field_planner_data = field_planner_data.reset_index(drop=True)
    field_name = "w_" + type.lower()
    print(field_name)
    field_planner_data = field_planner_data.rename(columns={"weight": field_name})

    return field_planner_data


def get_weights(source, final_planner):

    disease_data = get_field_weights(source, final_planner, "FilteredAppendedDisease", "Disease")
    agency_data = get_field_weights(source, final_planner, "FilteredAppendedFirm", "Agency")
    drug_data = get_field_weights(source, final_planner, "FilteredAppendedDrug", "Drug")
    trial_data = get_field_weights(source, final_planner, "Trial_Acronym", "Trial")
    moa_data = get_field_weights(source, final_planner, "FilteredAppendedDrugClass", "MOA")
    
    final_output = pd.merge(pd.merge(pd.merge(pd.merge(disease_data, agency_data), drug_data),
                                     trial_data), moa_data)

    return final_output

def calculate_final_score(field_planner):
    return field_planner['w_disease'] + field_planner['w_agency'] + field_planner['w_drug'] + field_planner[
        'w_trial'] + field_planner['w_moa'] 

# Append Diseases, Drugs, Firms, Trials & MoA (Annotation + Trial)
final_planner['AppendedDisease'] = combine_annotation_trials(final_planner,"Disease","DiseaseFromTrials")
final_planner['AppendedDrug'] = combine_annotation_trials(final_planner,"Drug","DrugFromTrials")
final_planner['AppendedFirm'] = combine_annotation_trials(final_planner,"Agency","FirmFromTrials")
final_planner['AppendedDrugClass'] = combine_annotation_trials(final_planner,"Drug Class","DrugClassFromTrials")


# Filter the appended Diseases, Drugs, Firms, Trials & MoA (Annotation + Trial)
FilteredDisease, FilteredDrug, FilteredFirm, FilteredDrugClass,FilterAgency,FilteredPatientSubgroups = filter_all_fields()
final_planner = final_planner.merge(FilteredDisease, on = "Internal ID", how = "left")
final_planner = final_planner.merge(FilteredDrug, on = "Internal ID", how = "left")
final_planner = final_planner.merge(FilteredFirm, on = "Internal ID", how = "left")
final_planner = final_planner.merge(FilteredDrugClass, on = "Internal ID", how = "left")
final_planner = final_planner.merge(FilteredPatientSubgroups, on = "Internal ID", how = "left")



# Mark Industry Sponsered Abstracts (Filtered Annotation Firms From KG)
final_planner = final_planner.merge(FilterAgency, on = "Internal ID", how = "left")
final_planner = final_planner.rename(columns={'FilteredAgency':'IndustrySponseredAbs'})
final_planner['IndustrySponseredAbs'].fillna("nan",inplace=True)
final_planner['IsIndustrySponseredAbs'] = np.where(final_planner['IndustrySponseredAbs'] != "nan" ,1,0)


# Mark Priority Sessions
priority_session_type = ["Plenary Session","Oral Abstract Session"]
final_planner['IsPrioritySessionType'] = np.where(final_planner['Session Type'].str.contains("|".join(priority_session_type)) == True, 1,0)

#Add weights
# planner_weights = get_weights(filter, final_planner)
# final_planner = final_planner.merge(planner_weights, on = "Internal ID", how = "left")
# final_planner['KeywordWeight'] = final_planner.apply(calculate_final_score, axis = 1)


print(final_planner.columns.values)
final_planner = final_planner.replace("nan"," ")
final_planner.sort_values(by='Sort', ascending=True, inplace=True)
final_planner.to_csv(r'final-planner.csv', index = False)
print(final_planner['Internal ID'].count())
print(final_planner['v2Priority'].count())

