import pandas as pd
import numpy as np

pd.options.mode.chained_assignment = None

# Input Data - Base files
matrix = pd.read_csv('matrix.csv', encoding="latin1", low_memory=False)
kg_data = pd.read_csv('Stable_KG_Mappings.csv', encoding="utf-8", low_memory=False)
trials_db = pd.read_csv('trialdb.csv', encoding="utf-8", low_memory=False)
no_catalyst_matrix = pd.read_csv('nocatalystmatrix.csv', encoding="utf-8", low_memory=False)
sort = pd.read_csv('sort.csv', encoding="latin1", low_memory=False)

# Input Data Manual
planner_raw_data = pd.read_csv('planner_raw_data.csv', encoding="utf-8", low_memory=False)
print(planner_raw_data.columns.values)

planner_melt_data = pd.read_csv('planner_melt_data.csv', encoding="latin1", low_memory=False)
planner_annotation_data = pd.read_csv('planner_annotation_data.csv', encoding="latin1", low_memory=False)
filter = pd.read_csv('filter.csv', encoding="latin1", low_memory=False)

#All diseases in annotation
disease_input_id = ("SDS001","SDS002")
print(disease_input_id)

TA_input = 'Oncology'
TA_input_id = 'DIS000002'
print(TA_input_id)

# Filter KG and Trial data based on Disease Input
def get_filtered_df(source, input, field):
    source_data_filtered = pd.DataFrame()
    for x in input:
        temp_source_data = source[source[field] == x]
        source_data_filtered = source_data_filtered.append(temp_source_data)
    return source_data_filtered


# kg_data = get_filtered_df(kg_data, disease_input_id, "k_disease_id")
# kg_data.to_csv(r'filteredKG.csv', index = False)
#
# trials_db = get_filtered_df(trials_db, disease_input_id,"disease_id")
# trials_db.to_csv(r'filteredTRIAL.csv', index = False)


# Create list of Source IDs and TAs
def get_TA():
    planner_TA = planner_annotation_data[planner_annotation_data['source_id'] == TA_input_id][["Internal ID", "name"]]
    planner_TA.rename(columns={'name': 'TA'}, inplace=True)
    return planner_TA


# Create a list of Source IDs and Diseases
def get_all_indications():
    planner_indications = planner_annotation_data[planner_annotation_data['label'] == 'Disease'][
        ["Internal ID", "source_id"]]
    planner_indications.rename(columns={'source_id': 'indication_id'}, inplace=True)
    return planner_indications


# Create a matrix based on values in a row
def generateMatrix(interimCol, finalCol):
    planner[interimCol] = planner['label'].apply(lambda x: 1 if x == finalCol else 0)
    planner[finalCol] = planner.groupby('Internal ID')[interimCol].transform(sum)
    planner[finalCol] = planner[finalCol].apply(lambda x: 1 if x > 0 else 0)
    return planner[finalCol]


# Columns into comma seperated values
def get_kg_entities():
    disease = planner_drug_disease[planner_drug_disease['label'] == 'Disease'][['Internal ID', 'name']]
    disease['Disease'] = disease.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    disease = disease[['Internal ID', 'Disease']].drop_duplicates()

    drug = planner_drug_disease[planner_drug_disease['label'] == 'Drug'][['Internal ID', 'name']]
    drug['Drug'] = drug.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    drug = drug[['Internal ID', 'Drug']].drop_duplicates()

    agency = planner_drug_disease[planner_drug_disease['label'] == 'Agency'][['Internal ID', 'name']]
    agency['Agency'] = agency.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    agency = agency[['Internal ID', 'Agency']].drop_duplicates()

    drug_classes = planner_drug_disease[planner_drug_disease['label'] == 'MOA'][['Internal ID', 'name']]
    drug_classes['Drug Class'] = drug_classes.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    drug_classes = drug_classes[['Internal ID', 'Drug Class']].drop_duplicates()

    patient_subgroups = planner_drug_disease[planner_drug_disease['label'] == 'PatientSubGroup'][['Internal ID', 'name']]
    patient_subgroups['Patient Subgroup'] = patient_subgroups.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    patient_subgroups = patient_subgroups[['Internal ID', 'Patient Subgroup']].drop_duplicates()


    return disease, drug, agency, drug_classes,patient_subgroups


# Show from where annotation Picked up the disease
def getDiseaseSource():
    diseasesource = pd.DataFrame()
    for x in disease_input_id:
        temp_df = planner_annotation_data[planner_annotation_data['source_id'] == x]
        diseasesource = diseasesource.append(temp_df)

    diseasesource = diseasesource[['Internal ID', 'field']]
    diseasesource['source'] = diseasesource.groupby(['Internal ID'])['field'].transform(lambda x: ', '.join(x))
    diseasesource = diseasesource[['Internal ID', 'source']].drop_duplicates()

    return diseasesource


# Generate single row matrix from annotation
def generateMatrixANNO(interimCol, finalCol):
    planner_raw_data_annotation[interimCol] = planner_raw_data_annotation['label'].apply(
        lambda x: 1 if x == finalCol else 0)
    planner_raw_data_annotation[finalCol] = planner_raw_data_annotation.groupby('Internal ID')[interimCol].transform(
        sum)
    planner_raw_data_annotation[finalCol] = planner_raw_data_annotation[finalCol].apply(lambda x: 1 if x > 0 else 0)

    return planner_raw_data_annotation[finalCol]


# Generate single row catalyst matrix
def generate_catalyst_matrix(interimCol, finalCol):
    planner_melt_data[interimCol] = planner_melt_data['Catalyst Type/Topics'].apply(lambda x: 1 if x == finalCol else 0)
    planner_melt_data[finalCol] = planner_melt_data.groupby('Internal ID')[interimCol].transform(sum)
    planner_melt_data[finalCol] = planner_melt_data[finalCol].apply(lambda x: 1 if x > 0 else 0)
    return planner_melt_data[finalCol]


# Generate catalyst priority matrix
def generate_catalyst_priority():
    planner_melt_data['Trial Readouts'] = generate_catalyst_matrix('I_Trial Readouts', 'Trial Readouts')
    planner_melt_data['Clinical Trial Details'] = generate_catalyst_matrix('I_Clinical Trial Details',
                                                                           'Clinical Trial Details')
    planner_melt_data['FDA Review Outcomes'] = generate_catalyst_matrix('I_FDA Review Outcomes',
                                                                        'FDA Review Outcomes')
    planner_melt_data['Endpoints'] = generate_catalyst_matrix('I_Endpoints', 'Endpoints')
    planner_melt_data['KOL'] = generate_catalyst_matrix('I_KOL', 'KOL')
    planner_melt_data['New Entries'] = generate_catalyst_matrix('I_New Entries', 'New Entries')
    planner_melt_data['Recruitment status'] = generate_catalyst_matrix('I_Recruitment status',
                                                                       'Recruitment status')
    planner_melt_data['Educational and Research'] = generate_catalyst_matrix(
        'I_Educational and Research', 'Educational and Research')
    planner_melt_data['Disease Statistic'] = generate_catalyst_matrix('I_Disease Statistic',
                                                                      'Disease Statistic')
    planner_melt_data['Treatment Details'] = generate_catalyst_matrix('I_Treatment Details',
                                                                      'Treatment Details')
    planner_melt_data['Drug Application'] = generate_catalyst_matrix('I_Drug Application', 'Drug Application')
    planner_melt_data['Preclinical'] = generate_catalyst_matrix('I_Preclinical', 'Preclinical')
    planner_melt_data['Late-Breaking Abstracts'] = generate_catalyst_matrix('I_Late-Breaking Abstracts',
                                                                            'Late-Breaking Abstracts')
    return planner_melt_data


# v2 Priortization Logic
def finalPriority(planner_raw_data):
    if planner_raw_data['Updated_Relevance'] == 1:
        if planner_raw_data['v1Priority'] != 'Ignore' and planner_raw_data['v1Priority'] != 'Not Relevant':
            return planner_raw_data['v1Priority']
        else:
            return planner_raw_data['NoCatalystPriority']
    else:
        # Add TA in the casing present in TA mapping file
        if planner_raw_data['TA'] == TA_input:
            if planner_raw_data['NoCatalystPriority'] == "Not Relevant":
                return "Ignore"
            elif planner_raw_data['NoCatalystPriority'] is not np.nan:
                return planner_raw_data['NoCatalystPriority']
            else:
                return "Ignore"
        else:
            return "Not Relevant"


# Convert single row
def singleRow(col):
    planner_raw_data[col] = planner_raw_data.groupby('Internal ID')[col].transform(sum)
    planner_raw_data[col] = planner_raw_data[col].apply(lambda x: 1 if x > 0 else 0)
    return planner_raw_data[col]


# Perform Generic OR Brand
def f(planner_raw_data):
    return planner_raw_data['CheckedGeneric'] or planner_raw_data['CheckedBrand']


# Filter required columns
planner = planner_raw_data[['Internal ID','Abstract ID','Session Type', 'Session Name', 'Abstract Title', 'Session Start Date','Author','Institution']]

# Add Relevance Based on Input Disease Area
planner_indications = get_all_indications()
planner_raw_data = planner_raw_data.merge(planner_indications, on='Internal ID', how='left')
planner_raw_data['Relevance'] = planner_raw_data['indication_id'].apply(lambda x: 1 if x in disease_input_id else 0)
planner_raw_data.sort_values(by='Relevance', ascending=False, inplace=True)
planner_raw_data.drop_duplicates(subset='Internal ID', keep='first', inplace=True)

# Generate Catalyst Priority
planner_melt_data = generate_catalyst_priority()
planner_melt_data = planner_melt_data[
    ['Internal ID', 'Catalyst Type/Topics', 'Trial Readouts', 'Clinical Trial Details', 'FDA Review Outcomes',
     'Endpoints', 'KOL', 'New Entries', 'Recruitment status', 'Educational and Research',
     'Disease Statistic', 'Treatment Details', 'Drug Application', 'Preclinical', 'Late-Breaking Abstracts']]
planner_melt_data.drop_duplicates(
    subset={'Internal ID', 'Trial Readouts', 'Clinical Trial Details', 'FDA Review Outcomes', 'Endpoints', 'KOL',
            'New Entries', 'Recruitment status', 'Educational and Research',
            'Disease Statistic', 'Treatment Details', 'Drug Application', 'Preclinical', 'Late-Breaking Abstracts'},
    keep='first', inplace=True)
planner_melt_data = planner_melt_data.merge(matrix,
                                            on=['Trial Readouts', 'Clinical Trial Details', 'FDA Review Outcomes',
                                                'Endpoints', 'KOL',
                                                'New Entries', 'Recruitment status', 'Educational and Research',
                                                'Disease Statistic', 'Treatment Details', 'Drug Application',
                                                'Preclinical',
                                                'Late-Breaking Abstracts'], how='left')

# Add Catalyst Priority & v1Priority
planner_raw_data = planner_raw_data.merge(planner_melt_data, on='Internal ID', how='left')
planner_raw_data.rename(columns={'Priority': 'Catalyst_Priority'}, inplace=True)
planner_raw_data.to_csv(r'checkCatalystPriority.csv', index = False)

planner_raw_data['v1Priority'] = np.where(planner_raw_data.Relevance == 1, planner_raw_data.Catalyst_Priority,
                                          "Not Relevant")
planner_raw_data['v1Priority'] = np.where(planner_raw_data.v1Priority != " ", planner_raw_data.v1Priority, "Ignore")
planner_raw_data['v1Priority'].replace(np.nan, "Ignore", inplace=True)
planner_raw_data = planner_raw_data.loc[:,
                   ['Internal ID','Abstract ID','Session Type', 'Session Name', 'Abstract Title', 'Session Start Date','Author','Institution', 'Relevance',
                    'Catalyst_Priority', 'v1Priority']]

# Attach TA
planner_TA = get_TA()
planner_raw_data = planner_raw_data.merge(planner_TA, on='Internal ID', how='left')

# Add Annotation Results
planner = planner.merge(planner_annotation_data, on='Internal ID', how='left')
planner['Disease'] = generateMatrix('S_Diseases', 'Disease')
planner['Drug'] = generateMatrix('S_Drugs', 'Drug')
planner['Firm'] = generateMatrix('S_Firms', 'Firms')
planner['Trial'] = generateMatrix('S_Trials', 'Trial')
planner['DrugClass'] = generateMatrix('S_DrugClass', 'MOA')

# Get All KG entities
planner_drug_disease = planner[['Internal ID', 'name', 'label']]
A_Diseases, A_Drugs, A_Agency, A_DrugClasses,A_PatientSubgroups = get_kg_entities()

# Attach Trial Data
planner_annotation_data_trials = planner_annotation_data[planner_annotation_data['label'] == 'Trial']
planner_raw_data = planner_raw_data.merge(planner_annotation_data_trials, on='Internal ID', how='left')
planner_raw_data = planner_raw_data.merge(trials_db, left_on='source_id', right_on='nct_id', how='left')
planner_raw_data['drug_id'].fillna(0, inplace=True)
planner_raw_data['drugclass_id'].fillna(0, inplace=True)
planner_raw_data['firm_id'].fillna(0, inplace=True)

# Check if attached trial data is the one present in KG
planner_raw_data = planner_raw_data.assign(CheckedDisease=planner_raw_data.disease_id.isin(kg_data.k_disease_id))
planner_raw_data = planner_raw_data.assign(CheckedGeneric=planner_raw_data.drug_id.isin(kg_data.k_generic_id))
planner_raw_data = planner_raw_data.assign(CheckedBrand=planner_raw_data.drug_id.isin(kg_data.k_brand_id))
planner_raw_data = planner_raw_data.assign(CheckedFirm=planner_raw_data.firm_id.isin(kg_data.k_firm_id))
planner_raw_data = planner_raw_data.assign(CheckedDrugclass=planner_raw_data.drugclass_id.isin(kg_data.k_drug_class_id))

# Keep the field as it is if disease of interest else 0
planner_raw_data['CheckedGeneric'] = np.where(planner_raw_data["CheckedDisease"] == True,
                                              planner_raw_data["CheckedGeneric"], 0)
planner_raw_data['CheckedBrand'] = np.where(planner_raw_data["CheckedDisease"] == True,
                                            planner_raw_data["CheckedBrand"], 0)
planner_raw_data['CheckedFirm'] = np.where(planner_raw_data["CheckedDisease"] == True, planner_raw_data["CheckedFirm"],
                                           0)
planner_raw_data['CheckedDrugclass'] = np.where(planner_raw_data["CheckedDisease"] == True,
                                                planner_raw_data["CheckedDrugclass"], 0)

# Change True to 1 and False to 0
planner_raw_data['CheckedDisease'] = planner_raw_data['CheckedDisease'].apply(lambda x: 1 if x == True else 0)
planner_raw_data['CheckedFirm'] = planner_raw_data['CheckedFirm'].apply(lambda x: 1 if x == True else 0)
planner_raw_data['CheckedGeneric'] = planner_raw_data['CheckedGeneric'].apply(lambda x: 1 if x == True else 0)
planner_raw_data['CheckedBrand'] = planner_raw_data['CheckedBrand'].apply(lambda x: 1 if x == True else 0)
planner_raw_data['CheckedDrugclass'] = planner_raw_data['CheckedDrugclass'].apply(lambda x: 1 if x == True else 0)

# Single out the row and sort then Keep the field as it is if disease of interest else 0
planner_raw_data['CheckedDisease'] = singleRow('CheckedDisease')
planner_raw_data['CheckedFirm'] = singleRow('CheckedFirm')
planner_raw_data['CheckedFirm'] = np.where(planner_raw_data["CheckedDisease"] == 1, singleRow('CheckedFirm'), 0)
planner_raw_data['CheckedGeneric'] = singleRow('CheckedGeneric')
planner_raw_data['CheckedGeneric'] = np.where(planner_raw_data["CheckedDisease"] == 1, singleRow('CheckedGeneric'), 0)
planner_raw_data['CheckedBrand'] = singleRow('CheckedBrand')
planner_raw_data['CheckedBrand'] = np.where(planner_raw_data["CheckedDisease"] == 1, singleRow('CheckedBrand'), 0)
planner_raw_data['CheckedDrugclass'] = singleRow('CheckedDrugclass')
planner_raw_data['CheckedDrugclass'] = np.where(planner_raw_data["CheckedDisease"] == 1, singleRow('CheckedDrugclass'),
                                                0)

planner_raw_data['CheckedDrug'] = planner_raw_data.apply(f, axis=1)
planner_raw_data['CheckedDrug'] = planner_raw_data['CheckedDrug'].apply(lambda x: 1 if x == True else 0)
planner_raw_data['CheckedDrug'] = singleRow('CheckedDrug')

planner_raw_data['CheckedTrial'] = planner_raw_data['disease_id'].apply(lambda x: 0 if x not in disease_input_id else 1)
planner_raw_data['DiseaseFromTrialCheck'] = planner_raw_data['disease_id'].apply(
    lambda x: 1 if x in disease_input_id else 0)
planner_raw_data.sort_values(['DiseaseFromTrialCheck', 'CheckedTrial', 'firm_id', 'drug_id', 'drugclass_id'],
                             ascending=[False, False, False, False, False], inplace=True)
planner_raw_data.drop_duplicates(subset={'Internal ID'}, inplace=True, keep='first')

# If disease of interest keep else np.nan
planner_raw_data['Disease'] = np.where(planner_raw_data.DiseaseFromTrialCheck == 1, planner_raw_data.disease, np.nan)
planner_raw_data['Agency'] = np.where(planner_raw_data.DiseaseFromTrialCheck == 1, planner_raw_data.firm, np.nan)
planner_raw_data['Drug'] = np.where(planner_raw_data.DiseaseFromTrialCheck == 1, planner_raw_data.drug, np.nan)
planner_raw_data['acronym'] = np.where(planner_raw_data.DiseaseFromTrialCheck == 1, planner_raw_data.acronym, np.nan)
planner_raw_data['DrugClass'] = np.where(planner_raw_data.DiseaseFromTrialCheck == 1, planner_raw_data.drugclass,
                                         np.nan)

print(planner_raw_data.columns.values)
planner_raw_data.to_csv(r'checkAbstractIDissue.csv', index = False)
planner_raw_data = planner_raw_data[['Internal ID','Abstract ID','Session Type', 'Session Name', 'Abstract Title', 'Session Start Date','Author','Institution', 'Relevance',
     'Catalyst_Priority', 'v1Priority', 'TA', 'Disease', 'Agency', 'Drug', 'DrugClass', 'acronym', 'CheckedDisease',
     'CheckedFirm', 'CheckedDrug', 'CheckedDrugclass', 'CheckedTrial', 'DiseaseFromTrialCheck']]
planner_raw_data.rename(columns={'Disease': 'DiseaseFromTrials', 'Agency': 'FirmFromTrials', 'Drug': 'DrugFromTrials',
                                 'acronym': 'Trial_Acronym', 'DrugClass': 'DrugClassFromTrials'}, inplace=True)

# Append & Filter Annotation Results
planner_raw_data = planner_raw_data.merge(planner_annotation_data, on='Internal ID', how='left')
planner_raw_data_annotation = planner_raw_data
planner_raw_data_annotation = planner_raw_data_annotation[(planner_raw_data_annotation.source_id.isin(filter.source_id))]

planner_raw_data_annotation['A_Disease'] = generateMatrixANNO('S_Diseases', 'Disease')
planner_raw_data_annotation['A_Drug'] = generateMatrixANNO('S_Drugs', 'Drug')
planner_raw_data_annotation['A_Firm'] = generateMatrixANNO('S_Firms', 'Agency')
planner_raw_data_annotation['A_Trial'] = generateMatrixANNO('S_Trials', 'Trial')
planner_raw_data_annotation['A_DrugClass'] = generateMatrixANNO('A_DrugClass', 'MOA')
planner_raw_data_annotation.sort_values(by='Trial', ascending=False, inplace=True)
planner_raw_data_annotation.drop_duplicates(subset={'Internal ID'}, inplace=True)

# Perform Annotation OR Trial Data
planner_raw_data_annotation[
    'Final_Disease'] = planner_raw_data_annotation.A_Disease | planner_raw_data_annotation.CheckedDisease
planner_raw_data_annotation['Final_Drug'] = planner_raw_data_annotation.A_Drug | planner_raw_data_annotation.CheckedDrug
planner_raw_data_annotation['Final_Firm'] = planner_raw_data_annotation.A_Firm | planner_raw_data_annotation.CheckedFirm
planner_raw_data_annotation[
    'Final_Trial'] = planner_raw_data_annotation.A_Trial | planner_raw_data_annotation.CheckedTrial
planner_raw_data_annotation[
    'Final_DrugClass'] = planner_raw_data_annotation.A_DrugClass | planner_raw_data_annotation.CheckedDrugclass

planner_raw_data_annotation.drop_duplicates(subset={'Internal ID'}, inplace=True)
planner_raw_data_annotation.rename(
    columns={'Final_Disease': 'NC_Disease', 'Final_Drug': 'NC_Drug', 'Final_Firm': 'NC_Firm', 'Final_Trial': 'NC_Trial',
             'Final_DrugClass': 'NC_DrugClass'}, inplace=True)

# Generate Priority
planner_raw_data_annotation = planner_raw_data_annotation.merge(no_catalyst_matrix,
                                                                on=['NC_Disease', 'NC_Drug', 'NC_Firm', 'NC_Trial',
                                                                    'NC_DrugClass'], how='left')
planner_raw_data_annotation.drop_duplicates(subset={'Internal ID'}, inplace=True)
planner_raw_data_annotation = planner_raw_data_annotation.loc[:,
                              ['Internal ID', 'Priority', 'NC_Disease', 'NC_Drug', 'NC_Firm', 'NC_Trial',
                               'NC_DrugClass']]

# Merge planner_raw_data_annotation with planner_raw_data
planner_raw_data = planner_raw_data.loc[:,
                   ['Internal ID','Abstract ID','Session Type', 'Session Name', 'Abstract Title', 'Session Start Date','Author','Institution', 'Relevance',
                    'Catalyst_Priority', 'v1Priority', 'TA', 'DiseaseFromTrials', 'FirmFromTrials', 'DrugFromTrials',
                    'DrugClassFromTrials', 'Trial_Acronym']]
planner_raw_data = planner_raw_data.merge(planner_raw_data_annotation, on='Internal ID', how='left')
planner_raw_data.drop_duplicates(subset={'Internal ID'}, inplace=True)
planner_raw_data.rename(columns={'Priority': 'NoCatalystPriority'}, inplace=True)

# Update Disease Relevance Post Implicit Trial Connections
planner_raw_data['CheckedDiseaseFromTrial'] = planner_raw_data['DiseaseFromTrials'].apply(
    lambda x: 1 if x in disease_input_id else 0)
planner_raw_data['Updated_Relevance'] = planner_raw_data['Relevance'] | planner_raw_data['CheckedDiseaseFromTrial']

# Apply v2 Priortization logic
planner_raw_data['v2Priority'] = planner_raw_data.apply(finalPriority, axis=1)

# Append Annotation Outputs - A_Diseases, A_Drugs, A_Agency & A_DrugClasses
planner_raw_data = pd.merge(planner_raw_data, A_Diseases, 'left', 'Internal ID')
planner_raw_data = pd.merge(planner_raw_data, A_Drugs, 'left', 'Internal ID')
planner_raw_data = pd.merge(planner_raw_data, A_Agency, 'left', 'Internal ID')
planner_raw_data = pd.merge(planner_raw_data, A_DrugClasses, 'left', 'Internal ID')
planner_raw_data = pd.merge(planner_raw_data, A_PatientSubgroups, 'left', 'Internal ID')

# Append disease source from annotation
disease_source = getDiseaseSource()
planner_raw_data = planner_raw_data.merge(disease_source, on='Internal ID', how='left')

# Sort & Export Planner
planner_raw_data = planner_raw_data.merge(sort, on='v2Priority', how='left')
planner_raw_data.sort_values(by='Sort', ascending=True, inplace=True)
planner_raw_data.to_csv(r'step1_planner.csv', index=False)
print(planner_raw_data['Internal ID'].count())
print(planner_raw_data['v2Priority'].count())


