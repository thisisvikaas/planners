import pandas as pd

final_planner = pd.read_csv(r'step1_planner.csv', encoding="utf-8")
kg_data = pd.read_csv(r'Stable_KG_Mappings.csv', encoding="utf-8",low_memory=False)
kg_drugs = pd.read_csv(r'kg_filter_drugs.csv', encoding = "utf-8")



# Split Just by the Disease of Interest
kg_data = kg_data[(kg_data['k_disease'] == 'Breast Cancer') | (kg_data['k_disease'] == 'Breast Carcinoma') | (kg_data['k_disease'] == 'Triple Negative Breast cancer') |
(kg_data['k_disease'] == 'Inflammatory Breast Cancer') | (kg_data['k_disease'] == 'Inflammatory Carcinoma of Breast') | (kg_data['k_disease'] == 'Lobular Breast Carcinoma')]

# Split the Disease Column using comma (,)
final_planner_disease = final_planner['Disease'].str.split(',', expand=True)
final_planner_disease['Internal ID'] = final_planner['Internal ID']
final_planner_disease = pd.melt(final_planner_disease,id_vars='Internal ID')
final_planner_disease = final_planner_disease[~final_planner_disease['value'].isnull()]
final_planner_disease = pd.merge(final_planner,final_planner_disease,'left','Internal ID')
final_planner_disease['value'] = final_planner_disease['value'].str.strip()
final_matched_diseases = final_planner_disease[(final_planner_disease.value.isin(kg_data['k_disease']))]
final_matched_diseases = final_matched_diseases[['Internal ID', 'value']]
final_matched_diseases.to_csv(r'splitdiseases.csv',index = False)


# Split the DRUG Column using comma (,)
final_planner_drug = final_planner['Drug'].str.split(',', expand=True)
final_planner_drug['Internal ID'] = final_planner['Internal ID']
final_planner_drug = pd.melt(final_planner_drug,id_vars='Internal ID')
final_planner_drug = final_planner_drug[~final_planner_drug['value'].isnull()]
final_planner_drug.value = final_planner_drug['value'].str.strip()
# Only Drug Names
final_planners_only_drugs = final_planner_drug[['value']]
final_planners_only_drugs.to_csv(r'justdrugsfrommelt.csv', index = False)
final_planner_drug.to_csv(r'splitdrugs.csv', index = False)