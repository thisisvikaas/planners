import pandas as pd

final_planner = pd.read_csv(r'justdrugsfrommelt.csv', encoding="utf-8")
kg_data = pd.read_csv(r'CheckAnnotatedOutput.csv', encoding="utf-8",low_memory=False)

print(len(final_planner))

final_planner.rename(columns = {'value':'k_drug'}, inplace = True)
final_matched_drug = pd.merge(final_planner,kg_data[['k_drug_id','k_drug']], on = ['k_drug'], how = 'left')
#final_matched_drug.drop_duplicates(subset={'k_drug_id','k_drug'}, inplace=True)
final_matched_drug = final_matched_drug[['k_drug_id','k_drug']]
print(len(final_matched_drug))

final_matched_drug.to_csv(r'drugswithids.csv', index = False)


