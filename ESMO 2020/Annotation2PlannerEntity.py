import pandas as pd
import numpy as np

pd.options.mode.chained_assignment = None

# Input Data
planner = pd.read_csv('planner_raw_data.csv', encoding="utf-8", low_memory=False)
planner_annotation_data = pd.read_csv('planner_annotation_data.csv', encoding="utf-8", low_memory=False)

# Columns into comma seperated values
def get_kg_entities():
    disease = planner_annotation_data[planner_annotation_data['label'] == 'Disease'][['Internal ID', 'name']]
    disease['Disease'] = disease.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    disease = disease[['Internal ID', 'Disease']].drop_duplicates()

    drug = planner_annotation_data[planner_annotation_data['label'] == 'Drug'][['Internal ID', 'name']]
    drug['Drug'] = drug.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    drug = drug[['Internal ID', 'Drug']].drop_duplicates()

    agency = planner_annotation_data[planner_annotation_data['label'] == 'Agency'][['Internal ID', 'name']]
    agency['Agency'] = agency.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    agency = agency[['Internal ID', 'Agency']].drop_duplicates()

    drug_classes = planner_annotation_data[planner_annotation_data['label'] == 'MOA'][['Internal ID', 'name']]
    drug_classes['Drug Class'] = drug_classes.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    drug_classes = drug_classes[['Internal ID', 'Drug Class']].drop_duplicates()

    patient_subgroups = planner_annotation_data[planner_annotation_data['label'] == 'PatientSubGroup'][['Internal ID', 'name']]
    patient_subgroups['Patient Subgroup'] = patient_subgroups.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    patient_subgroups = patient_subgroups[['Internal ID', 'Patient Subgroup']].drop_duplicates()

    trial = planner_annotation_data[planner_annotation_data['label'] == 'Trial'][['Internal ID', 'name']]
    trial['Trial'] = trial.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    trial = trial[['Internal ID', 'Trial']].drop_duplicates()

    trial_endpoints = planner_annotation_data[planner_annotation_data['label'] == 'TrialEndPoint'][['Internal ID', 'name']]
    trial_endpoints['TrialEndPoint'] = trial_endpoints.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    trial_endpoints = trial_endpoints[['Internal ID', 'TrialEndPoint']].drop_duplicates()


    return disease, drug, agency, drug_classes,patient_subgroups,trial,trial_endpoints


# Filter required columns
A_Diseases, A_Drugs, A_Agency, A_DrugClasses,A_PatientSubgroups,A_Trial,A_TrialEndpoint = get_kg_entities()

# Append Annotation Outputs 
planner = pd.merge(planner, A_Diseases, 'left', 'Internal ID')
planner = pd.merge(planner, A_Drugs, 'left', 'Internal ID')
planner = pd.merge(planner, A_Agency, 'left', 'Internal ID')
planner = pd.merge(planner, A_DrugClasses, 'left', 'Internal ID')
planner = pd.merge(planner, A_PatientSubgroups, 'left', 'Internal ID')
planner = pd.merge(planner, A_Trial, 'left', 'Internal ID')
planner = pd.merge(planner, A_TrialEndpoint, 'left', 'Internal ID')

print("**********Annotation Results Added*****************")
print(planner['Internal ID'].count())
planner.to_csv(r'step1-planner.csv', index=False)











