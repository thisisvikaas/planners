import pandas as pd
import numpy as np

# Input
final_planner = pd.read_csv(r'step1-planner.csv', encoding="utf-8")
final_planner.fillna("nan", inplace=True)


def filter_by_kg(df, field):
    matched_field = df[field].str.split(', ', expand=True)
    matched_field['Internal ID'] = df['Internal ID']
    matched_field = pd.melt(matched_field, id_vars='Internal ID')
    matched_field = matched_field[~matched_field['value'].isnull()]
    matched_field = pd.merge(df, matched_field, 'left', 'Internal ID')

    matched_field['value'] = matched_field['value'].str.strip()
    matched_field.drop_duplicates(subset={"Internal ID", "value"}, inplace=True)
    matched_field = matched_field.dropna()

    filtered_field = matched_field
    # filtered_field = matched_field[(matched_field.value.isin(filter.name))]
    filtered_field = filtered_field[['Internal ID', 'value']]

    field_name = "Filtered" + field
    #print(field_name)
    filtered_field = filtered_field.rename(columns={"value": field_name})
    filtered_field = filtered_field.groupby(["Internal ID"])[field_name].agg([(field_name, ', '.join)]).reset_index()
    #filtered_field.to_csv(f"fiter-{field_name}.csv", index=False)

    return filtered_field


# Filter the Appended KG entities
def filter_all_fields():
    FilteredDisease = filter_by_kg(final_planner, 'Disease')
    FilteredDrug = filter_by_kg(final_planner, 'Drug')
    FilteredFirm = filter_by_kg(final_planner, 'Agency')
    FilteredDrugClass = filter_by_kg(final_planner, 'Drug Class')
    FilteredPatientSubgroup = filter_by_kg(final_planner, "Patient Subgroup")
    FilteredTrial = filter_by_kg(final_planner, 'Trial')
    FilteredTrialEndpoint = filter_by_kg(final_planner, "TrialEndPoint")

    return FilteredDisease, FilteredDrug, FilteredFirm, FilteredDrugClass, FilteredPatientSubgroup, FilteredTrial, FilteredTrialEndpoint

# Filter the appended Diseases, Drugs, Firms, Trials & MoA (Annotation + Trial)
FilteredDisease, FilteredDrug, FilteredFirm, FilteredDrugClass, FilteredPatientSubgroup, FilteredTrial, FilteredTrialEndpoint = filter_all_fields()
final_planner = final_planner.merge(FilteredDisease, on="Internal ID", how="left")
final_planner = final_planner.merge(FilteredDrug, on="Internal ID", how="left")
final_planner = final_planner.merge(FilteredFirm, on="Internal ID", how="left")
final_planner = final_planner.merge(FilteredDrugClass, on="Internal ID", how="left")
final_planner = final_planner.merge(FilteredPatientSubgroup, on="Internal ID", how="left")
final_planner = final_planner.merge(FilteredTrial, on="Internal ID", how="left")
final_planner = final_planner.merge(FilteredTrialEndpoint, on="Internal ID", how="left")




print(final_planner.columns.values)
final_planner = final_planner.replace("nan", " ")
final_planner.to_csv(r'final-planner.csv', index=False)
print(final_planner['Internal ID'].count())

