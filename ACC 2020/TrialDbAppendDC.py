import pandas as pd
import numpy as np


# Input Data
planner_raw_data = pd.read_csv('planner_raw_data.csv', encoding="latin1",low_memory=False)
planner_melt_data = pd.read_csv('planner_melt_data.csv', encoding="latin1",low_memory=False)
matrix = pd.read_csv('matrix.csv', encoding="latin1",low_memory=False)
planner_annotation_data = pd.read_csv('planner_annotation_data.csv', encoding="latin1",low_memory=False)
kg_data = pd.read_csv('Stable_KG_Mappings.csv', encoding="latin1",low_memory=False)
disease_ta = pd.read_csv('disease_ta.csv', encoding="utf-8",low_memory=False)
filter = pd.read_csv('filter.csv', encoding="latin1",low_memory=False)
trials_db = pd.read_csv('trialdb.csv', encoding="utf-8",low_memory=False)
nocatalystmatrix = pd.read_csv('nocatalystmatrix.csv', encoding="utf-8",low_memory=False)
sort = pd.read_csv('sort.csv', encoding="latin1", low_memory = False)



print(trials_db.columns.values)
print(kg_data.columns.values)

trials_db = trials_db.merge(kg_data, left_on={'Disease_id','Drug_id'}, right_on={'k_disease_id','k'})