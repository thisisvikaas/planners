import pandas as pd

kg_data = pd.read_csv('filteredKG.csv', encoding="latin1", low_memory=False)
trials_db = pd.read_csv('trialdb.csv', encoding="utf-8", low_memory=False)

disease_input = ("Heart Failure","Cardiomyopathy")
input_id = tuple(kg_data.loc[kg_data['k_disease'] == x, 'k_disease_id'].iloc[0] for x in disease_input)


print(input)
print(input_id)


def prepare_filter():
    interim_diseases = kg_data.loc[:,["k_disease_id", "k_disease"]]
    interim_diseases.drop_duplicates(subset={"k_disease", "k_disease_id"}, inplace=True)
    interim_diseases.rename(columns={'k_disease_id': 'source_id', 'k_disease': 'name'}, inplace=True)
    interim_diseases["label"] = "Disease"

    interim_agencies = kg_data.loc[:,["parent_firm_id", "parent_firm"]]
    interim_agencies.drop_duplicates(subset={"parent_firm_id", "parent_firm"}, inplace=True)
    interim_agencies.rename(columns={'parent_firm_id': 'source_id', 'parent_firm': 'name'}, inplace=True)
    interim_agencies["label"] = "Agency"

    interim_generics = kg_data.loc[:,["k_generic_id", "k_generic"]]
    interim_generics.drop_duplicates(subset={"k_generic", "k_generic_id"}, inplace=True)
    interim_generics.rename(columns={'k_generic_id': 'source_id', 'k_generic': 'name'}, inplace=True)
    interim_generics["label"] = "Drug"

    interim_brands = kg_data.loc[:,["k_brand_id", "k_brand"]]
    interim_brands.drop_duplicates(subset={"k_brand_id", "k_brand"}, inplace=True)
    interim_brands.rename(columns={'k_brand_id': 'source_id', 'k_brand': 'name'}, inplace=True)
    interim_brands["label"] = "Drug"
    interim_drugs = interim_generics.append(interim_brands)

    interim_trials = get_trials()
    
    return interim_diseases, interim_agencies, interim_drugs, interim_trials


def get_diseases():
    interim_diseases = pd.DataFrame()
    for x in input_id:
        temp_diseases = kg_data[kg_data['k_disease_id'] == x]
        interim_diseases = interim_diseases.append(temp_diseases)
    interim_diseases = interim_diseases.loc[:,["k_disease_id", "k_disease"]]
    interim_diseases.drop_duplicates(subset={"k_disease", "k_disease_id"}, inplace=True)
    interim_diseases.rename(columns={'k_disease_id': 'source_id', 'k_disease': 'name'}, inplace=True)
    interim_diseases["label"] = "Disease"

    return interim_diseases


def get_agencies():
    interim_agencies = pd.DataFrame()
    for x in input_id:
        temp_agencies = kg_data[kg_data['k_disease_id'] == x]
        interim_agencies = interim_agencies.append(temp_agencies)
    interim_agencies = interim_agencies.loc[:,["parent_firm_id", "parent_firm"]]
    interim_agencies.drop_duplicates(subset={"parent_firm_id", "parent_firm"}, inplace=True)
    interim_agencies.rename(columns={'parent_firm_id': 'source_id', 'parent_firm': 'name'}, inplace=True)
    interim_agencies["label"] = "Agency"

    return interim_agencies


def get_drugs():
    interim_generics = pd.DataFrame()
    for x in input_id:
        temp_generics = kg_data[kg_data['k_disease_id'] == x]
        interim_generics = interim_generics.append(temp_generics)
    interim_generics = interim_generics.loc[:,["k_generic_id", "k_generic"]]
    interim_generics.drop_duplicates(subset={"k_generic", "k_generic_id"}, inplace=True)
    interim_generics.rename(columns={'k_generic_id': 'source_id', 'k_generic': 'name'}, inplace=True)
    interim_generics["label"] = "Drug"

    interim_brands = pd.DataFrame()
    for x in input_id:
        temp_brands = kg_data[kg_data['k_disease_id'] == x]
        interim_brands = interim_brands.append(temp_brands)
    interim_brands = interim_brands.loc[:,["k_brand_id", "k_brand"]]
    interim_brands.drop_duplicates(subset={"k_brand_id", "k_brand"}, inplace=True)
    interim_brands.rename(columns={'k_brand_id': 'source_id', 'k_brand': 'name'}, inplace=True)
    interim_brands["label"] = "Drug"

    interim_drugs = interim_generics.append(interim_brands)
    return interim_drugs


def get_trials():
    interim_trials = pd.DataFrame()
    for x in input_id:
        temp_trials = trials_db[trials_db['disease_id'] == x]
        interim_trials = interim_trials.append(temp_trials)
    interim_trials = interim_trials.loc[:,["nct_id", "acronym"]]
    interim_trials.drop_duplicates(subset={"nct_id", "acronym"}, inplace=True)
    interim_trials.rename(columns={'nct_id': 'source_id', 'acronym': 'name'}, inplace=True)
    interim_trials["label"] = "Trial"

    return interim_trials

def append_keywords(diseases, agencies, drugs, trials):
    keywords = diseases.append(agencies)
    keywords = keywords.append(drugs)
    keywords = keywords.append(trials)
    
    return keywords

def clean_keywords(keywords):
    keywords.drop_duplicates(subset={'source_id', 'name'}, inplace=True)
    keywords.fillna(" ", inplace=True)
    keywords = keywords[(keywords['source_id'] != " ") | (keywords['name'] != " ")]
    
    return keywords

filtered_diseases = get_diseases()
filtered_agencies = get_agencies()
filtered_drugs = get_drugs()
filtered_trials = get_trials()

filtered_kg = append_keywords(filtered_diseases,filtered_agencies,filtered_drugs,filtered_trials)
filtered_kg = clean_keywords(filtered_kg)


appended_diseases,appended_agencies,appended_drugs,appended_trials = prepare_filter()
appened_kg = append_keywords(appended_diseases,appended_agencies,appended_drugs,appended_trials)
appened_kg = clean_keywords(appened_kg)

#filtered_kg.to_csv(r'kg_filter.csv', index=False)
#print(filtered_kg['source_id'].count())

appened_kg.to_csv(r'kg_appended_filter.csv', index = False)
print(appened_kg['source_id'].count())