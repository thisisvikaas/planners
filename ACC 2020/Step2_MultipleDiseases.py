import pandas as pd
import Planner_Step1

final_planner = pd.read_csv(r'step1_planner.csv', encoding="utf-8")
kg_data = Planner_Step1.kg_data

print(len(kg_data))




# Split the Disease Column using comma (,)
final_planner_disease = final_planner['Disease'].str.split(',', expand=True)
final_planner_disease['Internal ID'] = final_planner['Internal ID']
final_planner_disease = pd.melt(final_planner_disease,id_vars='Internal ID')
final_planner_disease = final_planner_disease[~final_planner_disease['value'].isnull()]
final_planner_disease = pd.merge(final_planner,final_planner_disease,'left','Internal ID')
final_planner_disease['value'] = final_planner_disease['value'].str.strip()
final_matched_diseases = final_planner_disease[(final_planner_disease.value.isin(kg_data['k_disease']))]
final_matched_diseases = final_matched_diseases[['Internal ID', 'value']]
final_matched_diseases.to_csv(r'splitdiseases.csv',index = False)


# Split the DRUG Column using comma (,)
final_planner_drug = final_planner['Drug'].str.split(',', expand=True)
final_planner_drug['Internal ID'] = final_planner['Internal ID']
final_planner_drug = pd.melt(final_planner_drug,id_vars='Internal ID')
final_planner_drug = final_planner_drug[~final_planner_drug['value'].isnull()]
final_planner_drug.value = final_planner_drug['value'].str.strip()
final_planners_only_drugs = final_planner_drug[['value']]
final_planners_only_drugs.to_csv(r'justdrugsfrommelt.csv', index = False)
final_planner_drug.to_csv(r'splitdrugs.csv', index = False)


# Split the Firm Column using comma (,)
final_planner_firm = final_planner['Agency'].str.split(',', expand=True)
final_planner_firm['Internal ID'] = final_planner['Internal ID']
final_planner_firm = pd.melt(final_planner_firm,id_vars='Internal ID')
final_planner_firm.to_csv(r'splitfirms1.csv',index = False)
final_planner_firm = final_planner_firm[~final_planner_firm['value'].isnull()]
final_planner_firm = pd.merge(final_planner,final_planner_firm,'left','Internal ID')
final_planner_firm['value'] = final_planner_firm['value'].str.strip()
final_matched_firms = final_planner_firm[(final_planner_firm.value.isin(kg_data['k_firm']))]
final_matched_firms = final_matched_firms[['Internal ID', 'value']]
final_matched_firms.to_csv(r'splitfirms.csv',index = False)


# Split the Drug Class Column using comma (,)
final_planner_drug_class = final_planner['Drug Class'].str.split(',', expand=True)
final_planner_drug_class['Internal ID'] = final_planner['Internal ID']
final_planner_drug_class = pd.melt(final_planner_drug_class,id_vars='Internal ID')
final_planner_drug_class = final_planner_drug_class[~final_planner_drug_class['value'].isnull()]
final_planner_drug_class = pd.merge(final_planner,final_planner_drug_class,'left','Internal ID')
final_planner_drug_class['value'] = final_planner_drug_class['value'].str.strip()
final_matched_drug_classs = final_planner_drug_class[(final_planner_drug_class.value.isin(kg_data['k_drug_class']))]
final_matched_drug_classs = final_matched_drug_classs[['Internal ID', 'value']]
final_matched_drug_classs.to_csv(r'splitdrugclass.csv',index = False)

