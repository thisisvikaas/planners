import pandas as pd

kg_data = pd.read_csv('Stable_KG_Mappings.csv', encoding="latin1", low_memory=False)
trials_db = pd.read_csv('trialdb.csv', encoding="utf-8", low_memory=False)

# disease_input = ("Heart Failure","Cardiomyopathy")
# input_id = tuple(kg_data.loc[kg_data['k_disease'] == x, 'k_disease_id'].iloc[0] for x in disease_input)
#
#
# print(input)
# print(input_id)


def prepare_filter():
    interim_diseases = kg_data.loc[:,["k_disease_id", "k_disease"]]
    interim_diseases.drop_duplicates(subset={"k_disease", "k_disease_id"}, inplace=True)
    interim_diseases.rename(columns={'k_disease_id': 'source_id', 'k_disease': 'name'}, inplace=True)
    interim_diseases["label"] = "Disease"

    interim_agencies = kg_data.loc[:,["k_firm_id", "k_firm"]]
    interim_agencies.drop_duplicates(subset={"k_firm_id", "k_firm"}, inplace=True)
    interim_agencies.rename(columns={'k_firm_id': 'source_id', 'k_firm': 'name'}, inplace=True)
    interim_agencies["label"] = "Agency"

    interim_generics = kg_data.loc[:,["k_generic_id", "k_generic"]]
    interim_generics.drop_duplicates(subset={"k_generic", "k_generic_id"}, inplace=True)
    interim_generics.rename(columns={'k_generic_id': 'source_id', 'k_generic': 'name'}, inplace=True)
    interim_generics["label"] = "Drug"

    interim_brands = kg_data.loc[:,["k_brand_id", "k_brand"]]
    interim_brands.drop_duplicates(subset={"k_brand_id", "k_brand"}, inplace=True)
    interim_brands.rename(columns={'k_brand_id': 'source_id', 'k_brand': 'name'}, inplace=True)
    interim_brands["label"] = "Drug"
    interim_drugs = interim_generics.append(interim_brands)

    #interim_trials = get_trials()
    
    return interim_diseases, interim_agencies, interim_drugs


def append_keywords(diseases, agencies, drugs):
    keywords = diseases.append(agencies)
    keywords = keywords.append(drugs)

    return keywords

def clean_keywords(keywords):
    keywords.drop_duplicates(subset={'source_id', 'name'}, inplace=True)
    keywords.fillna(" ", inplace=True)
    keywords = keywords[(keywords['source_id'] != " ") | (keywords['name'] != " ")]
    
    return keywords

# filtered_diseases = get_diseases()
# filtered_agencies = get_agencies()
# filtered_drugs = get_drugs()
# filtered_trials = get_trials()

# filtered_kg = append_keywords(filtered_diseases,filtered_agencies,filtered_drugs,filtered_trials)
# filtered_kg = clean_keywords(filtered_kg)
#

appended_diseases,appended_agencies,appended_drugs = prepare_filter()
appened_kg = append_keywords(appended_diseases,appended_agencies,appended_drugs)
appened_kg = clean_keywords(appened_kg)

#filtered_kg.to_csv(r'kg_filter.csv', index=False)
#print(filtered_kg['source_id'].count())

appened_kg.to_csv(r'filter.csv', index = False)
print(appened_kg['source_id'].count())