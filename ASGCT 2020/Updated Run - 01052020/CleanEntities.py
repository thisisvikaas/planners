import pandas as pd
import re


df = pd.read_csv('TEST1.csv', encoding="utf-8",low_memory=False)
print(df.columns.values)

#df = df[['Abstract ID', 'Title', 'Authors', 'Institutions', 'Abstract', 'Category']]

def strip_character(dataCol):
    r = re.compile(r'[^a-zA-Z !@#$%&*_+-=|\:";<>,./()[\]{}\']')
    return r.sub('', dataCol)

# #df['C_Internal ID'] = df['Internal ID'].apply(strip_character)
# df['C_Abstract Title'] = df['Abstract Title'].apply(strip_character)
# #df['Abstract ID'] = df['Abstract ID'].apply(strip_character)
# df['C_Authors'] = df['Authors'].apply(strip_character)
# #df['Poster ID'] = df['Poster ID'].apply(strip_character)
# df['C_Institutions'] = df['Institutions'].apply(strip_character)
df['C_Abstract_Content'] = df['Abstract_Content'].apply(strip_character)
# df['C_Category'] = df['Category'].apply(strip_character)


df.to_csv(r'ASGCT-Final-Data-CONTENT.csv', index = False)
