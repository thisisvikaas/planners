import pandas as pd

final_planner = pd.read_csv(r'nov_ms_planner.csv', encoding="latin1")
kg_neuro = pd.read_csv('CheckAnnotatedOutput.csv', encoding="latin1",low_memory=False)
input = "Multiple Sclerosis"

# Split Just by the Disease of Interest
kg_neuro = kg_neuro[(kg_neuro['k_disease'] == input)]

# Split the Disease Column using comma (,)
final_planner_disease = final_planner['Disease'].str.split(', ', expand=True)
final_planner_disease['Internal ID'] = final_planner['Internal ID']
final_planner_disease = pd.melt(final_planner_disease,id_vars='Internal ID')
final_planner_disease = final_planner_disease[~final_planner_disease['value'].isnull()]
final_planner_disease = pd.merge(final_planner,final_planner_disease,'left','Internal ID')
final_matched_diseases = final_planner_disease[(final_planner_disease.value.isin(kg_neuro['k_disease']))]
final_matched_diseases = final_matched_diseases[['Internal ID', 'value']]

# Split the DRUG Column using comma (,)
final_planner_drug = final_planner['Drug'].str.split(',', expand=True)
final_planner_drug['Internal ID'] = final_planner['Internal ID']
final_planner_drug = pd.melt(final_planner_drug,id_vars='Internal ID')
final_planner_drug = final_planner_drug[~final_planner_drug['value'].isnull()]
final_planner_drug.value = final_planner_drug['value'].str.strip()
final_matched_drug = pd.merge(final_planner,final_planner_drug,'left','Internal ID')
final_matched_drug = final_matched_drug[(final_matched_drug.value.isin(kg_neuro['k_drug']))]
final_matched_drug = final_matched_drug[['Internal ID', 'value']]
final_matched_drug = final_matched_drug.groupby(["Internal ID"])["value"].agg([('value',','.join)]).reset_index()
final_matched_drug.to_csv(r'finalmatcheddrug.csv', index = False)

final_planner = final_planner.merge(final_matched_diseases, on = 'Internal ID', how = 'left')

final_planner = final_planner.merge(final_matched_drug, on = 'Internal ID', how = 'left')
final_planner.rename(columns = {'value_x':'FilteredDiseases','value_y':'FilteredDrugs'}, inplace = True)


print(final_planner.columns.values)

#final_planner = final_planner.groupby('Internal ID')['FilteredDrugs'].agg([('FilteredDrugs', ', '.join)])

final_planner.sort_values(by='Sort', ascending=True, inplace=True)
final_planner.to_csv(r'final_nov_ms_planner.csv', index = False)




