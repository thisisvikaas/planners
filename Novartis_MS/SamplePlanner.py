import pandas as pd
import numpy as np

# Input Data
aan_data = pd.read_csv('aandata.csv', encoding="latin1",low_memory=False)
aanmelt = pd.read_csv('aanmeltoutput.csv', encoding="latin1",low_memory=False)
matrix = pd.read_csv('matrix.csv', encoding="latin1",low_memory=False)
aan_annotation = pd.read_csv('Annotation_AAN.csv', encoding="latin1",low_memory=False)
kg_neuro = pd.read_csv('KG_Neurology.csv', encoding="latin1",low_memory=False)
disease_ta = pd.read_csv('neurotamapping.csv', encoding="utf-8",low_memory=False)
filter = pd.read_csv('filter.csv', encoding="utf-8",low_memory=False)
trials_db = pd.read_csv('trialdb.csv', encoding="utf-8",low_memory=False)
nocatalystmatrix = pd.read_csv('nocatalystmatrix.csv', encoding="utf-8",low_memory=False)
sort = pd.read_csv('sort.csv', encoding="latin1", low_memory = False)


# Given a disease area provide the TA and its id in KG
input = "Multiple Sclerosis"
pd.options.mode.chained_assignment = None

# Preliminary Steps
# Create list of Source IDs and TAs

def get_TA():
    aan_TA = aan_annotation[aan_annotation['label'] == 'Disease'][['Internal ID', 'name', 'source_id']]
    aan_TA.rename(columns={'source_id': 'disease_id'}, inplace=True)
    aan_TA = aan_TA.merge(disease_ta, on='disease_id',how = 'left')
    aan_TA.sort_values(by='TA',inplace = True)
    aan_TA.drop_duplicates(subset={'Internal ID'}, keep='first', inplace=True)
    aan_TA = aan_TA[['Internal ID', 'TA']]
    return aan_TA

# Create a list of Source IDs and Diseases
def get_all_indications():
    aan_indications = aan_annotation[aan_annotation['label'] != '']
    aan_indications = aan_indications[aan_indications['label'] == 'Disease']
    aan_indications.rename(columns={'name': 'indication'}, inplace=True)
    aan_indications = aan_indications[['Internal ID', 'indication']]
    return aan_indications


# Create a matrix based on values in a row
def generateMatrix(interimCol, finalCol):
    aan[interimCol] = aan['label'].apply(lambda x: 1 if x == finalCol else 0)
    aan[finalCol] = aan.groupby('Internal ID')[interimCol].transform(sum)
    aan[finalCol] = aan[finalCol].apply(lambda x: 1 if x > 0 else 0)
    return aan[finalCol]

# Columns into comma seperated values
def getAllDiseasesAndDrugs():
    disease = aan_drug_disease[aan_drug_disease['label'] == 'Disease'][['Internal ID', 'name']]
    disease['Disease'] = disease.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    disease = disease[['Internal ID', 'Disease']].drop_duplicates()
    drug = aan_drug_disease[aan_drug_disease['label'] == 'Drug'][['Internal ID', 'name']]
    drug['Drug'] = drug.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    drug = drug[['Internal ID', 'Drug']].drop_duplicates()
    agency = aan_drug_disease[aan_drug_disease['label'] == 'Agency'][['Internal ID', 'name']]
    agency['Agency'] = agency.groupby(['Internal ID'])['name'].transform(lambda x: ', '.join(x))
    agency = agency[['Internal ID', 'Agency']].drop_duplicates()

    return disease,drug,agency

def generateMatrixANNO(interimCol, finalCol):
    aan_data_annotation[interimCol] = aan_data_annotation['label'].apply(lambda x: 1 if x == finalCol else 0)
    aan_data_annotation[finalCol] = aan_data_annotation.groupby('Internal ID')[interimCol].transform(sum)
    aan_data_annotation[finalCol] = aan_data_annotation[finalCol].apply(lambda x: 1 if x > 0 else 0)

    return aan_data_annotation[finalCol]


def generate_catalyst_matrix(interimCol, finalCol):
    aanmelt[interimCol] = aanmelt['Catalyst Type/Topics'].apply(lambda x: 1 if x == finalCol else 0)
    aanmelt[finalCol] = aanmelt.groupby('Internal ID')[interimCol].transform(sum)
    aanmelt[finalCol] = aanmelt[finalCol].apply(lambda x: 1 if x > 0 else 0)
    return aanmelt[finalCol]


def generate_catalyst_priority():
    aanmelt['Trial Readouts'] = generate_catalyst_matrix('I_Trial Readouts', 'Trial Readouts')
    aanmelt['Clinical Trial Details'] = generate_catalyst_matrix('I_Clinical Trial Details',
                                                               'Clinical Trial Details')
    aanmelt['FDA Review Outcomes'] = generate_catalyst_matrix('I_FDA Review Outcomes',
                                                            'FDA Review Outcomes')
    aanmelt['Endpoints'] = generate_catalyst_matrix('I_Endpoints', 'Endpoints')
    aanmelt['KOL'] = generate_catalyst_matrix('I_KOL', 'KOL')
    aanmelt['New Entries'] = generate_catalyst_matrix('I_New Entries', 'New Entries')
    aanmelt['Recruitment status'] = generate_catalyst_matrix('I_Recruitment status',
                                                           'Recruitment status')
    aanmelt['Educational and Research'] = generate_catalyst_matrix(
        'I_Educational and Research', 'Educational and Research')
    aanmelt['Disease Statistic'] = generate_catalyst_matrix('I_Disease Statistic',
                                                          'Disease Statistic')
    aanmelt['Treatment Details'] = generate_catalyst_matrix('I_Treatment Details',
                                                          'Treatment Details')
    aanmelt['Drug Application'] = generate_catalyst_matrix('I_Drug Application', 'Drug Application')
    aanmelt['Preclinical'] = generate_catalyst_matrix('I_Preclinical', 'Preclinical')
    aanmelt['Late-Breaking Abstracts'] = generate_catalyst_matrix('I_Late-Breaking Abstracts',
                                                                'Late-Breaking Abstracts')
    return aanmelt


def finalPriority(aan_data):
    if aan_data['Relevance'] == 1:
        if aan_data['v1Priority'] != 'Ignore':
            return aan_data['v1Priority']
        else:
            return aan_data['NoCatalystPriority']
    else:
        if aan_data['TA'] == "Neurology":
            if aan_data['NoCatalystPriority'] == "Not Relevant":
                return "Ignore"
            elif aan_data['NoCatalystPriority'] is not np.nan:
                return aan_data['NoCatalystPriority']
            else:
                return "Ignore"
        else:
            return "Not Relevant"


def singleRow(col):
    aan_data[col] = aan_data.groupby('Internal ID')[col].transform(sum)
    aan_data[col] = aan_data[col].apply(lambda x: 1 if x > 0 else 0)
    return aan_data[col]


def f(aan_data):
    return aan_data['CheckedGeneric'] or aan_data['CheckedBrand']



aan_data = aan_data[['Internal ID', 'Abstract Title', 'Session Name', 'Session Time','Session Type']]
# Add Relevance Based on Input Disease Area
aan_indications = get_all_indications()
aan_data = aan_data.merge(aan_indications, on='Internal ID', how='left')
aan_data['Relevance'] = aan_data['indication'].apply(lambda x: 1 if x == input else 0)
aan_data.sort_values(by = 'Relevance', ascending=False, inplace=True)
aan_data.drop_duplicates(subset='Internal ID', keep='first', inplace=True)
#aan_data.to_csv(r'aan_test1.csv',index = False)

# Generate Catalyst Priority

aanmelt = generate_catalyst_priority()
aanmelt = aanmelt[
        ['Internal ID', 'Catalyst Type/Topics', 'Trial Readouts', 'Clinical Trial Details', 'FDA Review Outcomes',
         'Endpoints', 'KOL', 'New Entries', 'Recruitment status', 'Educational and Research',
         'Disease Statistic', 'Treatment Details', 'Drug Application', 'Preclinical', 'Late-Breaking Abstracts']]
aanmelt.drop_duplicates(
        subset={'Internal ID', 'Trial Readouts', 'Clinical Trial Details', 'FDA Review Outcomes', 'Endpoints', 'KOL',
                'New Entries', 'Recruitment status', 'Educational and Research',
                'Disease Statistic', 'Treatment Details', 'Drug Application', 'Preclinical', 'Late-Breaking Abstracts'},
        keep='first', inplace=True)
aanmelt = aanmelt.merge(matrix,
                            on=['Trial Readouts', 'Clinical Trial Details', 'FDA Review Outcomes', 'Endpoints', 'KOL',
                                'New Entries', 'Recruitment status', 'Educational and Research',
                                'Disease Statistic', 'Treatment Details', 'Drug Application', 'Preclinical',
                                'Late-Breaking Abstracts'], how='left')

#aanmelt.to_csv(r'aan_melt_test1.csv', index = False)



aan_data = aan_data.merge(aanmelt, on = 'Internal ID', how = 'left')

aan_data.rename(columns={'Priority':'Catalyst_Priority'}, inplace=True)

aan_data['v1Priority'] = np.where(aan_data.Relevance == 1, aan_data.Catalyst_Priority, "Not Relevant")
aan_data['v1Priority'] = np.where(aan_data.v1Priority != " ", aan_data.v1Priority, "Ignore")
aan_data['v1Priority'].replace(np.nan,"Ignore",inplace = True)
aan_data = aan_data[['Internal ID', 'Abstract Title', 'Session Name', 'Session Time','Session Type','Relevance','Catalyst_Priority','v1Priority']]

# Attach TA
aan_TA = pd.DataFrame()
aan_TA = get_TA()
aan_TA.to_csv(r'TAfile.csv', index = False)
aan_data = aan_data.merge(aan_TA, on = 'Internal ID', how = 'left')


# Get All Drugs
aan = aan_data[['Internal ID', 'Session Type', 'Session Name', 'Abstract Title', 'Session Time']]
aan = aan.merge(aan_annotation, on='Internal ID', how='left')

# Step 2 - Append New Columns Disease,Drug, Firm, Trial, Catalysts and TA
# Non Catalysts
aan['Disease'] = generateMatrix('S_Diseases', 'Disease')
aan['Drug'] = generateMatrix('S_Drugs', 'Drug')
aan['Firm'] = generateMatrix('S_Firms', 'Firms')
aan['Trial'] = generateMatrix('S_Trials', 'Trial')

# Get All Drugs and Diseases
aan_drug_disease = aan[['Internal ID', 'name', 'label']]

aan_drug_disease.to_csv(r'checkdrugdisease.csv', index = False)
A_Diseases, A_Drugs, A_Agency= getAllDiseasesAndDrugs()


# Attach Trial Data

aan_annotation_trials = aan_annotation [aan_annotation['label'] == 'Trial']
aan_data = aan_data.merge(aan_annotation_trials, on = 'Internal ID', how = 'left')
aan_data = aan_data.merge(trials_db, on = 'source_id', how = 'left')

aan_data.to_csv(r'interimtrialoutput.csv', index = False)

# Now check with KG
# CheckedVariables Denote If Available in Kg
kg_neuro = kg_neuro[(kg_neuro['k_disease'] == input)]
kg_neuro.to_csv(r'kg_neuro_check.csv', index = False)
aan_data = aan_data.assign(CheckedGeneric = aan_data.Drug_id.isin(kg_neuro.k_generic_id))
aan_data = aan_data.assign(CheckedBrand = aan_data.Drug_id.isin(kg_neuro.k_brand_id))
aan_data = aan_data.assign(CheckedFirm = aan_data.Agency_id.isin(kg_neuro.k_firm_id))
aan_data = aan_data.assign(CheckedDisease = aan_data.Disease_id.isin(kg_neuro.k_disease_id))


aan_data['CheckedDrug'] = aan_data.apply(f,axis=1)

aan_data['CheckedDrug'] = aan_data['CheckedDrug'].apply(lambda x: 1 if x == True else 0)
aan_data['CheckedFirm'] = aan_data['CheckedFirm'].apply(lambda x: 1 if x == True else 0)
aan_data['CheckedDisease'] = aan_data['CheckedDisease'].apply(lambda x: 1 if x == True else 0)


aan_data['CheckedDrug'] = singleRow('CheckedDrug')
aan_data['CheckedFirm'] = singleRow('CheckedFirm')
aan_data['CheckedDisease'] = singleRow('CheckedDisease')
aan_data['CheckedTrial'] = aan_data['Disease_id'].apply(lambda x: 0 if x!="DIS000065"  else 1)

aan_data['DiseaseFromTrialCheck'] = aan_data['Disease'].apply(lambda x: 1 if x == input else 0)

aan_data.sort_values(['CheckedTrial','DiseaseFromTrialCheck'], ascending=[False,False], inplace = True)
aan_data.drop_duplicates(subset={'Internal ID'}, inplace=True)
aan_data.to_csv(r'interimtrialoutput2.csv', index = False)
#aan_data.to_csv(r'aan_test_4.csv', index = False)
aan_data.drop_duplicates(subset={'Internal ID'}, inplace=True)

aan_data = aan_data[['Internal ID', 'Abstract Title', 'Session Name', 'Session Time','Session Type','Relevance','Catalyst_Priority','v1Priority','TA','Disease','Agency','Drug','acronym','CheckedDisease','CheckedFirm','CheckedDrug','CheckedTrial']]
aan_data.rename(columns={'Disease':'DiseaseFromTrials','Agency':'FirmFromTrials','Drug':'DrugFromTrials','acronym':'Trial_Acronym'}, inplace=True)
#aan_data.to_csv(r'aan_test_5.csv', index = False)

# Append Annotation Results

aan_data = aan_data.merge(aan_annotation, on ='Internal ID', how = 'left')
#aan_data.to_csv(r'aan_test_6.csv', index = False)

# Assign to a new df since rows will be deleted
aan_data_annotation = aan_data

# aan_data_annotation = aan_data_annotation.merge(trials_db, on = 'source_id', how = 'left')
# aan_data_annotation.to_csv(r'finaltest.csv', index = False)

# To allow only our keywords
aan_data_annotation = aan_data_annotation[(aan_data_annotation.source_id.isin(filter.source_id)) & (aan_data_annotation.label.isin(filter.label))]

aan_data_annotation['A_Disease'] = generateMatrixANNO('S_Diseases', 'Disease')
aan_data_annotation['A_Drug'] = generateMatrixANNO('S_Drugs', 'Drug')
aan_data_annotation['A_Firm'] = generateMatrixANNO('S_Firms', 'Agency')
aan_data_annotation['A_Trial'] = generateMatrixANNO('S_Trials','Trial')

#aan_data_annotation.to_csv(r'aan_test_7.csv', index = False)

#print(aan_data_annotation.columns.values)

aan_data_annotation.sort_values(by='Trial', ascending=False, inplace=True)
aan_data_annotation.drop_duplicates(subset={'Internal ID'}, inplace = True)
aan_data_annotation.to_csv(r'aan_interim_annotatedplustrialcheck.csv', index = False)


aan_data_annotation ['Final_Disease'] = aan_data_annotation.A_Disease | aan_data_annotation.CheckedDisease
aan_data_annotation ['Final_Drug'] = aan_data_annotation.A_Drug | aan_data_annotation.CheckedDrug
aan_data_annotation ['Final_Firm'] = aan_data_annotation.A_Firm | aan_data_annotation.CheckedFirm
aan_data_annotation ['Final_Trial'] = aan_data_annotation.A_Trial | aan_data_annotation.CheckedTrial
aan_data_annotation.drop_duplicates(subset={'Internal ID'}, inplace = True)

aan_data_annotation.rename(columns={'Final_Disease' :'NC_Disease', 'Final_Drug':'NC_Drug','Final_Firm':'NC_Firm','Final_Trial':'NC_Trial'}, inplace = True)



aan_data_annotation = aan_data_annotation.merge(nocatalystmatrix,on=['NC_Disease', 'NC_Drug', 'NC_Firm', 'NC_Trial'], how = 'left')
aan_data_annotation.drop_duplicates(subset={'Internal ID'}, inplace = True)
aan_data_annotation = aan_data_annotation[['Internal ID', 'Priority']]

# Merge aan_data_annotaion with aan_Data
aan_data = aan_data[['Internal ID', 'Abstract Title', 'Session Name', 'Session Time','Session Type','Relevance','Catalyst_Priority','v1Priority','TA','DiseaseFromTrials','FirmFromTrials','DrugFromTrials','Trial_Acronym']]
aan_data = aan_data.merge(aan_data_annotation, on ='Internal ID', how = 'left')
aan_data.drop_duplicates(subset={'Internal ID'}, inplace=True)
aan_data.rename(columns={'Priority':'NoCatalystPriority'}, inplace = True)

# Apply v2 Priortization logic
aan_data['v2Priority'] = aan_data.apply(finalPriority, axis = 1)
aan_data = pd.merge(aan_data,A_Diseases,'left','Internal ID')
aan_data = pd.merge(aan_data,A_Drugs,'left','Internal ID')


aan_data = aan_data.merge(sort, on = 'v2Priority', how = 'left')
aan_data.sort_values(by='Sort', ascending=True, inplace=True)
# aan_data.drop_duplicates(subset = {'Internal ID', 'value_x', 'value_y'}, inplace=True)

aan_data.to_csv(r'nov_ms_planner.csv', index = False)








