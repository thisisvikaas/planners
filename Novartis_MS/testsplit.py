import pandas as pd

final_planner_drug = pd.read_csv(r'finalplannerdrug.csv', encoding="latin1")
kg_neuro = pd.read_csv('CheckAnnotatedOutput.csv', encoding="latin1",low_memory=False)
input = "Multiple Sclerosis"

print(final_planner_drug.columns.values)

# Split the DRUG Column using comma (,)
final_planner_drug = final_planner_drug[~final_planner_drug['Drug'].isnull()]
final_matched_drug = final_planner_drug[(final_planner_drug.Drug.isin(kg_neuro['k_drug']))]
final_matched_drug = final_matched_drug.groupby(["Internal ID"])["Drug"].agg([('Drug',','.join)]).reset_index()
final_matched_drug.to_csv(r'finalmatcheddrug2.csv', index = False)