import pandas as pd

final_planner = pd.read_csv(r'nov_scz_planner.csv', encoding="latin1")
kg_neuro = pd.read_csv('CheckAnnotatedOutput.csv', encoding="latin1",low_memory=False)


# Split Just by the Disease of Interest
kg_neuro = kg_neuro[kg_neuro['k_disease'] == "Schizophrenia"]

# Split the Disease Column using comma (,)
final_planner_disease = final_planner['Disease'].str.split(', ', expand=True)
final_planner_disease['Internal ID'] = final_planner['Internal ID']
final_planner_disease = pd.melt(final_planner_disease,id_vars='Internal ID')
final_planner_disease = final_planner_disease[~final_planner_disease['value'].isnull()]
final_planner_disease = pd.merge(final_planner,final_planner_disease,'left','Internal ID')
final_matched_diseases = final_planner_disease[(final_planner_disease.value.isin(kg_neuro['k_disease']))]
final_matched_diseases = final_matched_diseases[['Internal ID', 'value']]

# Split the DRUG Column using comma (,)
final_planner_drug = final_planner['Drug'].str.split(',', expand=True)
final_planner_drug['Internal ID'] = final_planner['Internal ID']
final_planner_drug = pd.melt(final_planner_drug,id_vars='Internal ID')
final_planner_drug = final_planner_drug[~final_planner_drug['value'].isnull()]
final_planner_drug = pd.merge(final_planner,final_planner_drug,'left','Internal ID')
final_matched_drug = final_planner_drug[(final_planner_drug.value.isin(kg_neuro['k_drug']))]
final_matched_drug = final_matched_drug[['Internal ID', 'value']]


final_planner = final_planner.merge(final_matched_diseases, on = 'Internal ID', how = 'left')
final_planner = final_planner.merge(final_matched_drug, on = 'Internal ID', how = 'left')
final_planner.drop_duplicates(subset= {'Internal ID', 'value_x', 'value_y'}, inplace=True)


final_planner.sort_values(by='Sort', ascending=True, inplace=True)
final_planner.to_csv(r'final_nov_scz_planner.csv', index = False)




