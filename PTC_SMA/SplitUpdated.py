import pandas as pd
import numpy as np
import re

final_planner = pd.read_csv(r'sabcs_bc_planner.csv', encoding="utf-8")
kg_data = pd.read_csv(r'Stable_KG_Mappings.csv', encoding="utf-8",low_memory=False)
kg_drugs = pd.read_csv(r'kg_filter_drugs.csv', encoding = "utf-8")
appended_drugs_ids = pd.read_csv(r'finalcheck.csv', encoding="utf-8")

input = "Spinal Muscular Atrophy"

# Split Just by the Disease of Interest
kg_data = kg_data[(kg_data['k_disease'] == input)]
# Split the Disease Column using comma (,)
final_planner_disease = final_planner['Disease'].str.split(',', expand=True)
final_planner_disease['Internal ID'] = final_planner['Internal ID']
final_planner_disease = pd.melt(final_planner_disease,id_vars='Internal ID')
final_planner_disease = final_planner_disease[~final_planner_disease['value'].isnull()]
final_planner_disease = pd.merge(final_planner,final_planner_disease,'left','Internal ID')
final_planner_disease['value'] = final_planner_disease['value'].str.strip()
final_planner_disease.to_csv(r'interimDis.csv', index=False)
# print(kg_data['k_disease'])

kg_data.to_csv(r'kgcheckdis.csv', index = False)
final_matched_diseases = final_planner_disease[(final_planner_disease.value.isin(kg_data['k_disease']))]
final_matched_diseases = final_matched_diseases[['Internal ID', 'value']]

# Split the DRUG Column using comma (,)
final_planner_drug = final_planner['Drug'].str.split(',', expand=True)
final_planner_drug['Internal ID'] = final_planner['Internal ID']
final_planner_drug = pd.melt(final_planner_drug,id_vars='Internal ID')
final_planner_drug = final_planner_drug[~final_planner_drug['value'].isnull()]
final_planner_drug.value = final_planner_drug['value'].str.strip()
print(len(final_planner_drug))
# Only Drug Names
final_planners_only_drugs = final_planner_drug[['value']]
final_planners_only_drugs.to_csv(r'justdrugsfrommelt.csv', index = False)
final_planner_drug = pd.merge(final_planner_drug, appended_drugs_ids, left_on = "value",right_on="k_drug" , how = 'left')
final_planner_drug.drop_duplicates(subset={'Internal ID','k_drug','k_drug_id'}, inplace=True)
print(len(final_planner_drug))
final_planner_drug.to_csv(r'drugswithIDs.csv', index = False)


final_matched_drug = pd.merge(final_planner,final_planner_drug,'left','Internal ID')
final_matched_drug.to_csv(r'checkfinalplanner1.csv', index = False)
# print(final_matched_drug.columns.values)
# print(kg_drugs.columns.values)
final_matched_drug = final_matched_drug[(final_matched_drug.k_drug_id.isin(kg_drugs['k_drug_id']))]
final_matched_drug.to_csv(r'checkfinalplanner2.csv', index = False)

final_matched_drug = final_matched_drug[['Internal ID', 'value']]
print(len(final_matched_drug))
final_matched_drug = final_matched_drug.groupby(["Internal ID"])["value"].agg([('value','$$'.join)]).reset_index()
print(len(final_matched_drug))
final_matched_drug.to_csv(r'finalmatcheddrug.csv', index = False)

final_planner = final_planner.merge(final_matched_diseases, on = 'Internal ID', how = 'left')
final_planner = final_planner.merge(final_matched_drug, on = 'Internal ID', how = 'left')
final_planner.rename(columns = {'value_x':'FilteredDiseases','value_y':'FilteredDrugs'}, inplace = True)

final_planner.drop_duplicates(subset={'Internal ID'}, inplace = True)

# final_planner['AppendedDrugs'] = final_planner['DrugFromTrials'].str.cat(final_planner['FilteredDrugs'],sep = "$$")
# final_planner['AppendedDisease'] = final_planner['DiseaseFromTrials'].str.cat(final_planner['FilteredDiseases'], sep = "$$")
# final_planner['AppendedDrugs'] = final_planner['FilteredDrugs'].str.cat(final_planner['DrugFromTrials'], sep = "$$")
# final_planner['AppendedDisease'] = final_planner['FilteredDiseases'].str.cat(final_planner['DiseaseFromTrials'], sep = "$$")
#
#
# df['period'] = df[['Year', 'quarter']].apply(lambda x: ''.join(x), axis=1)

final_planner['FilteredDiseases'] = final_planner['FilteredDiseases'].astype(str)
final_planner['FilteredDrugs'] = final_planner['FilteredDrugs'].astype(str)
final_planner['DiseaseFromTrials'] = final_planner['DiseaseFromTrials'].astype(str)
final_planner['DrugFromTrials'] = final_planner['DrugFromTrials'].astype(str)



final_planner['AppendedDisease'] = np.where(final_planner['FilteredDiseases']=="nan",final_planner['DiseaseFromTrials'],
                                            np.where(final_planner['DiseaseFromTrials']=="nan",final_planner['FilteredDiseases'],
                                                     np.where(final_planner['FilteredDiseases']==final_planner['DiseaseFromTrials'],final_planner['FilteredDiseases'],
                                                              final_planner[
                                                                  ['FilteredDiseases', 'DiseaseFromTrials']].apply(
                                                                  lambda x: '$$'.join(x), axis=1))))

final_planner['AppendedDrug'] = np.where(final_planner['FilteredDrugs']=="nan",final_planner['DrugFromTrials'],
                                            np.where(final_planner['DrugFromTrials']=="nan",final_planner['FilteredDrugs'],
                                                     np.where(final_planner['FilteredDrugs']==final_planner['DrugFromTrials'],final_planner['FilteredDrugs'],
                                                              final_planner[
                                                                  ['FilteredDrugs', 'DrugFromTrials']].apply(
                                                                  lambda x: '$$'.join(x), axis=1))))

print(len(final_planner))
final_planner.sort_values(by='Sort', ascending=True, inplace=True)
final_planner.to_csv(r'final_sabcs_bc_planner.csv', index = False)




