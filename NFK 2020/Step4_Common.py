import pandas as pd
import numpy as np

final_planner = pd.read_csv(r'step1_planner.csv', encoding="utf-8")
kg_drugs = pd.read_csv(r'filter_kg_drugs.csv', encoding = "utf-8")
appended_drugs_ids = pd.read_csv(r'drugswithids.csv', encoding="utf-8")
final_matched_diseases = pd.read_csv(r'splitdiseases.csv', encoding="utf-8")
final_planner_drug = pd.read_csv(r'splitdrugs.csv', encoding="utf-8")
final_matched_firms = pd.read_csv(r'splitfirms.csv', encoding = "utf-8")
final_matched_drugclass = pd.read_csv(r'splitdrugclass.csv', encoding="utf-8")

# Filter Drugs From Our KG
final_planner_drug = pd.merge(final_planner_drug, appended_drugs_ids, left_on = "value",right_on="k_drug" , how = 'left')
final_planner_drug.drop_duplicates(subset={'Internal ID','k_drug','k_drug_id'}, inplace=True)
final_matched_drug = pd.merge(final_planner,final_planner_drug,'left','Internal ID')
final_matched_drug = final_matched_drug[(final_matched_drug.k_drug_id.isin(kg_drugs['k_drug_id']))]
final_matched_drug = final_matched_drug[['Internal ID', 'value']]


final_matched_drug = final_matched_drug.groupby(["Internal ID"])["value"].agg([('value','$$'.join)]).reset_index()
final_matched_diseases = final_matched_diseases.groupby(["Internal ID"])["value"].agg([('value','$$'.join)]).reset_index()
final_matched_firms = final_matched_firms.groupby(["Internal ID"])["value"].agg([('value','$$'.join)]).reset_index()
final_matched_drugclass = final_matched_drugclass.groupby(["Internal ID"])["value"].agg([('value','$$'.join)]).reset_index()


final_planner = final_planner.merge(final_matched_diseases, on = 'Internal ID', how = 'left')
final_planner = final_planner.merge(final_matched_drug, on = 'Internal ID', how = 'left')
final_planner.rename(columns = {'value_x':'FilteredDiseases','value_y':'FilteredDrugs'}, inplace = True)

final_planner = final_planner.merge(final_matched_firms, on = 'Internal ID', how = 'left')
final_planner = final_planner.merge(final_matched_drugclass, on = 'Internal ID', how = 'left')
final_planner.rename(columns = {'value_x':'FilteredFirms','value_y':'FilteredDrugClasses'}, inplace = True)


final_planner.to_csv(r'final_planner_interimmatch1.csv', index = False)

final_planner.drop_duplicates(subset={'Internal ID','FilteredDiseases','FilteredDrugs','FilteredFirms','FilteredDrugClasses'}, inplace = True)

final_planner['FilteredDiseases'] = final_planner['FilteredDiseases'].astype(str)
final_planner['FilteredDrugs'] = final_planner['FilteredDrugs'].astype(str)
final_planner['FilteredFirms'] = final_planner['FilteredFirms'].astype(str)
final_planner['FilteredDrugClasses'] = final_planner['FilteredDrugClasses'].astype(str)


final_planner['DiseaseFromTrials'] = final_planner['DiseaseFromTrials'].astype(str)
final_planner['DrugFromTrials'] = final_planner['DrugFromTrials'].astype(str)

# Seperate Diseases and Drugs by $$
final_planner['AppendedDisease'] = np.where(final_planner['FilteredDiseases']=="nan",final_planner['DiseaseFromTrials'],
                                            np.where(final_planner['DiseaseFromTrials']=="nan",final_planner['FilteredDiseases'],
                                                     np.where(final_planner['FilteredDiseases']==final_planner['DiseaseFromTrials'],final_planner['FilteredDiseases'],
                                                              final_planner[
                                                                  ['FilteredDiseases', 'DiseaseFromTrials']].apply(
                                                                  lambda x: '$$'.join(x), axis=1))))

final_planner['AppendedDrug'] = np.where(final_planner['FilteredDrugs']=="nan",final_planner['DrugFromTrials'],
                                            np.where(final_planner['DrugFromTrials']=="nan",final_planner['FilteredDrugs'],
                                                     np.where(final_planner['FilteredDrugs']==final_planner['DrugFromTrials'],final_planner['FilteredDrugs'],
                                                              final_planner[
                                                                  ['FilteredDrugs', 'DrugFromTrials']].apply(
                                                                  lambda x: '$$'.join(x), axis=1))))

print("Total Rows in Planner {}".format(len(final_planner)))


final_planner.rename(columns={"Disease":"DiseasesFromAnnotation","Drug":"DrugsFromAnnotation","source":"SourceofDisease","AppendedDisease":"FinalDisease","AppendedDrug":"FinalDrug","FilteredDiseases" :"FilteredAnnotationDiseases","FilteredDrugs" :"FilteredAnnotationDrugs","Agency": "FirmsFromAnnotation","FilteredFirms":"FinalFirms","FilteredDrugClasses":"FinalDrugClasses"}, inplace = True)

# Remove Duplicates From Diseases, Drugs and Firms
final_planner_diseases = final_planner['FinalDisease'].str.split('$', expand=True)
final_planner_diseases['Internal ID'] = final_planner['Internal ID']
final_planner_diseases = pd.melt(final_planner_diseases,id_vars='Internal ID')
final_planner_diseases = final_planner_diseases[~final_planner_diseases['value'].isnull()]
final_planner_diseases.value = final_planner_diseases['value'].str.strip()
final_planner_diseases.drop_duplicates(subset={"Internal ID","value"}, inplace = True)
final_planner_diseases = final_planner_diseases.replace(r'^\s*$', np.nan, regex=True)
final_planner_diseases = final_planner_diseases.dropna()
final_planner_diseases.to_csv(r'finalplannerdisease.csv', index = False)
final_planner_diseases = final_planner_diseases.groupby(["Internal ID"])["value"].agg([('value','$$'.join)]).reset_index()

final_planner_drugs = final_planner['FinalDrug'].str.split('$', expand=True)
final_planner_drugs['Internal ID'] = final_planner['Internal ID']
final_planner_drugs = pd.melt(final_planner_drugs,id_vars='Internal ID')
final_planner_drugs = final_planner_drugs[~final_planner_drugs['value'].isnull()]
final_planner_drugs.value = final_planner_drugs['value'].str.strip()
final_planner_drugs.drop_duplicates(subset={"Internal ID","value"}, inplace = True)
final_planner_drugs = final_planner_drugs.replace(r'^\s*$', np.nan, regex=True)
final_planner_drugs = final_planner_drugs.dropna()
final_planner_drugs.to_csv(r'finalplannerdrugs.csv', index = False)
final_planner_drugs = final_planner_drugs.groupby(["Internal ID"])["value"].agg([('value','$$'.join)]).reset_index()

final_planner_firms = final_planner['FinalFirms'].str.split('$', expand=True)
final_planner_firms['Internal ID'] = final_planner['Internal ID']
final_planner_firms = pd.melt(final_planner_firms,id_vars='Internal ID')
final_planner_firms = final_planner_firms[~final_planner_firms['value'].isnull()]
final_planner_firms.value = final_planner_firms['value'].str.strip()
final_planner_firms.drop_duplicates(subset={"Internal ID","value"}, inplace = True)
final_planner_firms = final_planner_firms.replace(r'^\s*$', np.nan, regex=True)
final_planner_firms = final_planner_firms.dropna()
final_planner_firms.to_csv(r'finalplannerfirms.csv', index = False)
final_planner_firms = final_planner_firms.groupby(["Internal ID"])["value"].agg([('value','$$'.join)]).reset_index()


#FinalDrugClasses
final_planner_drugclasses = final_planner['FinalDrugClasses'].str.split('$', expand=True)
final_planner_drugclasses['Internal ID'] = final_planner['Internal ID']
final_planner_drugclasses = pd.melt(final_planner_drugclasses,id_vars='Internal ID')
final_planner_drugclasses = final_planner_drugclasses[~final_planner_drugclasses['value'].isnull()]
final_planner_drugclasses.value = final_planner_drugclasses['value'].str.strip()
final_planner_drugclasses.drop_duplicates(subset={"Internal ID","value"}, inplace = True)
final_planner_drugclasses = final_planner_drugclasses.replace(r'^\s*$', np.nan, regex=True)
final_planner_drugclasses = final_planner_drugclasses.dropna()
final_planner_drugclasses.to_csv(r'finalplannerdrugclasses.csv', index = False)
final_planner_drugclasses = final_planner_drugclasses.groupby(["Internal ID"])["value"].agg([('value','$$'.join)]).reset_index()



final_planner = final_planner.merge(final_planner_diseases, on = "Internal ID")
final_planner = final_planner.merge(final_planner_drugs, on = "Internal ID")
final_planner.rename(columns= {"value_x":"DiseaseFromAnnotation&Trial", "value_y":"DrugsFromAnnotation&Trial"}, inplace = True)
final_planner = final_planner.merge(final_planner_firms, on = "Internal ID")
final_planner = final_planner.merge(final_planner_drugclasses, on = "Internal ID")
final_planner.rename(columns= {"value_x":"IndustrySponseredAbstracts", "value_y":"FinalDrugClass"}, inplace = True)



final_planner = final_planner.drop('FinalDisease', 1)
final_planner = final_planner.drop('FinalDrug', 1)
final_planner = final_planner.drop('FinalFirms', 1)
final_planner = final_planner.drop('FinalDrugClasses',1)

final_planner.to_csv(r'final_planner_interimmatch2.csv', index = False)






final_planner['DiseaseFromAnnotation&Trial'] = final_planner['DiseaseFromAnnotation&Trial'].replace("nan","")
final_planner['DrugsFromAnnotation&Trial'] = final_planner['DrugsFromAnnotation&Trial'].replace("nan", "")
final_planner['IndustrySponseredAbstracts'] = final_planner['IndustrySponseredAbstracts'].replace("nan","")
final_planner['FinalDrugClass'] = final_planner['FinalDrugClass'].replace("nan","")


priority_session_type = "Plenary Session"

final_planner['IsIndustrySponseredAbs'] = np.where(final_planner['IndustrySponseredAbstracts'] != "" ,1,0)
final_planner['IsDrugClass'] = np.where(final_planner['FinalDrugClass'] != "",1,0)
final_planner['IsPrioritySessionType'] = np.where(final_planner['Session Type'] != priority_session_type,0,1 )



final_planner.sort_values(by='Sort', ascending=True, inplace=True)
final_planner.to_csv(r'final_planner.csv', index = False)



