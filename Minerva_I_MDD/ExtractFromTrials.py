import pandas as pd
import numpy as np

# Input Data
trial_db = pd.read_csv('trialdb.csv', encoding="utf-8",low_memory=False)
input = "Insomnia"
trial_db = trial_db[trial_db['Disease'] == input]

trial_ID = pd.DataFrame()
drug_id = pd.DataFrame()
agency_id = pd.DataFrame()
final_trial_filter = pd.DataFrame()


trial_ID['source_id'] = trial_db['source_id']
trial_ID['name'] = trial_db['acronym']
trial_ID['label'] = 'Trial'

agency_id['source_id'] = trial_db['Agency_id']
agency_id['name'] = trial_db['Agency']
agency_id['label'] = 'Agency'

drug_id['source_id'] = trial_db['Drug_id']
drug_id['name'] = trial_db['Drug']
drug_id['label'] = 'Drug'

final_trial_filter = trial_ID.append(agency_id, ignore_index=True)
final_trial_filter = final_trial_filter.append(drug_id, ignore_index=True)

final_trial_filter.drop_duplicates(subset={'source_id','label','name'}, inplace=True)
print(final_trial_filter.columns.values)

final_trial_filter.to_csv(r'final_filter_from_trial.csv', index=False)