import pandas as pd


# Input
final_planner = pd.read_csv(r'should_boost_v2.csv', encoding="utf-8")
filter = pd.read_csv(r'filter-should.csv', encoding="utf-8")
print(final_planner.columns.values)


def filter_by_kg(df,field):
    matched_field = df[field].str.split('; ', expand=True)
    matched_field['Internal ID'] = df['Internal ID']
    matched_field = pd.melt(matched_field, id_vars='Internal ID')
    matched_field = matched_field[~matched_field['value'].isnull()]
    matched_field = pd.merge(df, matched_field, 'left', 'Internal ID')

    matched_field['value'] = matched_field['value'].str.strip()
    matched_field.drop_duplicates(subset={"Internal ID", "value"}, inplace=True)
    matched_field = matched_field.dropna()
    matched_field.to_csv(r'matchedshould.csv', index = False)

    filtered_field = matched_field
    filtered_field = matched_field[(~matched_field.value.str.lower().isin(filter.name.str.lower()))]
    filtered_field = filtered_field[['Internal ID', 'value']]

    field_name = "Filtered" + field
    print(field_name)
    filtered_field = filtered_field.rename(columns={"value": field_name})
    filtered_field = filtered_field.groupby(["Internal ID"])[field_name].agg([(field_name, '; '.join)]).reset_index()
    filtered_field.to_csv(f"fiter-{field_name}.csv", index=False)

    return filtered_field


def filter_all_fields():
    FilteredDisease = filter_by_kg(final_planner, 'should_boost')
    #FilteredDrugs = filter_by_kg(final_planner, 'Drug')
    return FilteredDisease

FilteredDisease = filter_all_fields()
final_planner = final_planner.merge(FilteredDisease, on = "Internal ID", how = "left")

final_planner.to_csv(r'ASCO-should-v2.csv', index = False)
