import pandas as pd

kg_data = pd.read_csv('Stable_KG_Mappings.csv', encoding="latin1",low_memory=False)
trials_db = pd.read_csv('trialdb.csv', encoding="utf-8",low_memory=False)

disease_input = ("Heart Failure","Cardiomyopathy","Ischemic Heart Disease","Congenital Heart Disease","Heart Valve Disease","Atrial Fibrillation","Pulmonary Hypertension","Myocardial Infarction","Coronary Artery Disease","Cardiovascular Disease","Heart Disease","Acute Coronary Syndrome","Hypertension","Aortic Valve Stenosis","Cardiogenic Shock","Mitral Valve Insufficiency","Ischemia","Amyloid Cardiomyopathy","Arrhythmia","Myocarditis","Endocarditis","Ventricular Tachycardia","Peripheral Arterial Disease","Cardiac Arrest","St-Elevation Myocardial Infarction","Atherosclerosis","Hypertrophic Cardiomyopathy","Pulmonary Embolism","Venous Thromboembolism","Bacterial Endocarditis","Artery Disease","Cardiotoxicity","Thrombosis","Pulmonary Arterial Hypertension","Tricuspid Valve Insufficiency","Angina","Chronic Thromboembolic Pulmonary Hypertension","Pericarditis","Congestive Heart Failure","Tachycardia","Rheumatic Heart Disease","Dilated Cardiomyopathy","Ventricular Fibrillation","Persistent Atrial Fibrillation","Right Ventricular Failure","Paroxysmal Atrial Fibrillation","Atrial Flutter","Ischemic Cardiomyopathy","Systolic Heart Failure","Supraventricular Tachycardia","Hypotension","Left Ventricular Dysfunction","Left Ventricular Hypertrophy","Thromboembolism","Hypoplastic Left Heart Syndrome","Nonvalvular Atrial Fibrillation","Post-Myocardial Infarction","Stable Angina","Takayasu's Arteritis","Orthostatic Hypotension","COronary Atherosclerosis","Non-Sustained Ventricular Tachycardia","Thoracic Aortic Aneurysm","Vasculitis","Giant Cell Arteritis","Deep Vein Thrombosis","Congenital Heart Defects","Non-Obstructive Hypertrophic Cardiomyopathy","Obstructive Hypertrophic Cardiomyopathy","Pulmonary Thromboembolism","Septic Shock","Angina Pectoris","Aortic Aneurysm","Arteritis","Intermittent Claudication","Portal Hypertension","Postural Orthostatic Tachycardia Syndrome","Venous Thrombosis","Arterial Hypertension","Arterial Thromboembolism","Arteriosclerosis","Coronary Arteriosclerosis","Familial Amyloid Cardiomyopathy","Familial Chylomicronemia Syndrome","Familial Partial Lipodystrophy","Idiopathic Hypertension","Occlusive Arterial Disease","Paroxysmal Ventricular Tachycardia","Persistent Pulmonary Hypertension of the Newborn","Systemic Embolism","Thrombotic Microangiopathy","Transient Ischemic Attack","Cardiology")
input_id = tuple(kg_data.loc[kg_data['k_disease'] == x, 'k_disease_id'].iloc[0] for x in disease_input)

print(disease_input)
print(input_id)


def get_diseases():
    interim_diseases = pd.DataFrame()
    for x in input_id:
        temp_diseases = kg_data[kg_data['k_disease_id'] == x]
        interim_diseases = interim_diseases.append(temp_diseases)
    interim_diseases = interim_diseases[["k_disease_id","k_disease"]]
    interim_diseases.drop_duplicates(subset={"k_disease","k_disease_id"}, inplace = True)
    interim_diseases.rename(columns = {'k_disease_id':'source_id', 'k_disease':'name'}, inplace=True)
    interim_diseases["label"] = "Disease"
    
    return interim_diseases


def get_agencies():
    interim_agencies = pd.DataFrame()
    for x in input_id:
        temp_agencies = kg_data[kg_data['k_disease_id'] == x]
        interim_agencies = interim_agencies.append(temp_agencies)
    interim_agencies = interim_agencies[["k_firm_id", "k_firm"]]
    interim_agencies.drop_duplicates(subset={"k_firm", "k_firm_id"}, inplace=True)
    interim_agencies.rename(columns={'k_firm_id': 'source_id', 'k_firm': 'name'}, inplace=True)
    interim_agencies["label"] = "Agency"

    return interim_agencies

def get_drugclass():
    interim_agencies = pd.DataFrame()
    for x in input_id:
        temp_agencies = kg_data[kg_data['k_disease_id'] == x]
        interim_agencies = interim_agencies.append(temp_agencies)
    interim_agencies = interim_agencies[["k_drug_class_id", "k_drug_class"]]
    interim_agencies.drop_duplicates(subset={"k_drug_class", "k_drug_class_id"}, inplace=True)
    interim_agencies.rename(columns={'k_drug_class_id': 'source_id', 'k_drug_class': 'name'}, inplace=True)
    interim_agencies["label"] = "MOA"

    return interim_drugclass



def get_drugs():
    interim_generics = pd.DataFrame()
    for x in input_id:
        temp_generics = kg_data[kg_data['k_disease_id'] == x]
        interim_generics = interim_generics.append(temp_generics)
    interim_generics = interim_generics[["k_generic_id", "k_generic"]]
    interim_generics.drop_duplicates(subset={"k_generic", "k_generic_id"}, inplace=True)
    interim_generics.rename(columns={'k_generic_id': 'source_id', 'k_generic': 'name'}, inplace=True)
    interim_generics["label"] = "Drug"

    interim_brands = pd.DataFrame()
    for x in input_id:
        temp_brands = kg_data[kg_data['k_disease_id'] == x]
        interim_brands = interim_brands.append(temp_brands)
    interim_brands = interim_brands[["k_brand_id", "k_brand"]]
    interim_brands.drop_duplicates(subset={"k_brand_id", "k_brand"}, inplace=True)
    interim_brands.rename(columns={'k_brand_id': 'source_id', 'k_brand': 'name'}, inplace=True)
    interim_brands["label"] = "Drug"

    interim_drugs = interim_generics.append(interim_brands)
    return interim_drugs



def get_trials():
    interim_trials = pd.DataFrame()
    for x in input_id:
        temp_trials = trials_db[trials_db['disease_id'] == x]
        interim_trials = interim_trials.append(temp_trials)
    interim_trials = interim_trials[["nct_id", "acronym"]]
    interim_trials.drop_duplicates(subset={"nct_id", "acronym"}, inplace=True)
    interim_trials.rename(columns={'nct_id': 'source_id', 'acronym': 'name'}, inplace=True)
    interim_trials["label"] = "Trial"

    return interim_trials


my_diseases = get_diseases()
my_agencies = get_agencies()
my_drugs = get_drugs()
my_trials = get_trials()
#my_drugclass = get_drugclass()

keywords = my_diseases.append(my_agencies)
keywords = keywords.append(my_drugs)
keywords = keywords.append(my_trials)
#keywords = keywords.append(my_drugclass)


keywords.to_csv(r'kg_filter.csv', index = True)
